#pragma once

// Algorithm from https://github.com/jwagner/smartcrop.js/

#include <QImage>

namespace SmartCrop {

struct CropOptions {
    CropOptions(const QSize &targetSize) : width(targetSize.width()), height(targetSize.height()) {}
    CropOptions() = default;

    qreal width = 0;
    qreal height= 0;
    static constexpr qreal aspect= 0;
    qreal cropWidth= 0;
    qreal cropHeight= 0;
    static constexpr qreal detailWeight= 0.2;
    static constexpr qreal skinColor[3] = {0.78, 0.57, 0.44};
    static constexpr qreal skinBias= 0.01;
    static constexpr qreal skinBrightnessMin= 0.2;
    static constexpr qreal skinBrightnessMax= 0.9;
    static constexpr qreal skinThreshold= 0.8;
    static constexpr qreal skinWeight= 1.8;
    static constexpr qreal saturationBrightnessMin= 0.05;
    static constexpr qreal saturationBrightnessMax= 0.9;
    static constexpr qreal saturationThreshold= 0.4;
    static constexpr qreal saturationBias= 0.2;
    static constexpr qreal saturationWeight= 0.1;
    // Step * minscale rounded down to the next power of two should be good
    static constexpr qreal scoreDownSample= 8;
    static constexpr qreal step= 8;
    static constexpr qreal scaleStep= 0.1;
    qreal minScale= 1.0;
    static constexpr qreal maxScale= 1.0;
    static constexpr qreal edgeRadius= 0.4;
    static constexpr qreal edgeWeight= -20.0;
    static constexpr qreal outsideImportance= -0.5;
    static constexpr qreal boostWeight= 100.0;
    static constexpr bool ruleOfThirds= true;
    static constexpr bool prescale= true;
};

QImage crop(const QImage &input, CropOptions options);
QRect smartCropRect(const QImage &input, CropOptions options);
}
