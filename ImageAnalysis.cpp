#include "ImageAnalysis.h"
#include "Settings.h"

#include "Convolve.h"

#include <QPixmap>
#include <QPainter>
#include <QElapsedTimer>
#include <QDebug>
#include <QFileInfo>
#include <QProgressDialog>
#include <QCoreApplication>
#include <QColorSpace>
#include <QGenericMatrix>
#include <QtMath>

#include <bitset>

#define BATCH_SIZE 10

//#define DEBUG_DUPES

template<int N>
constexpr QGenericMatrix<N, N, float> ph_dct_matrix() {
    QGenericMatrix<N, N, float> ret;
    ret.fill(1./qSqrt(N));
    const float c1 = sqrt(2.0/N);
    for (int x=0;x<N;x++){
        for (int y=1;y<N;y++){
            ret(x,y) = c1*cos((M_PI/2/N)*y*(2*x+1));
        }
    }
    return ret;
}

template<int N>
uint64_t calcImageHash2(const QImage &inputImage, const QString &filePath)
{
    static const QGenericMatrix<N, N, float> C = ph_dct_matrix<N>();
    static const QGenericMatrix<N, N, float> Ctransp = C.transposed();

    QGenericMatrix<N, N, float> img;
    const QImage grayImg = inputImage.scaled(N, N).convertToFormat(QImage::Format_Grayscale8);
    for (int y=0; y<grayImg.height(); y++) {
        const uchar *pixel = grayImg.constScanLine(y);
        for (int x=0; x<grayImg.width(); x++) {
            img(x, y) = pixel[x];
        }
    }

    const QGenericMatrix<N, N, float> dctImage = C * img * Ctransp;

    std::bitset<64> bitset;
    for (int y=0; y<8; y++) {
        for (int x=0; x<8; x++) {
            bitset.set(y * 8 + x, dctImage(x, y) > dctImage(x + 1, y));
        }
    }

    return bitset.to_ullong();
}

uint64_t ImageAnalysis::calcImageHash(const QImage &inputImage, const QString &filePath)
{
    QElapsedTimer t; t.start();
    QString filename = QFileInfo(filePath).fileName();

    if (inputImage.isNull()) {
        return 0;
    }
    QImage image = inputImage;

#ifdef DEBUG_DUPES
    image.save("/tmp/input-" + filename + ".png");
#endif

    image = inputImage.scaled(ANALYZE_SIZE/2, ANALYZE_SIZE/2, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

#ifdef DEBUG_DUPES
    image.save("/tmp/normscale-" + filename + ".png");
#endif

    // fill in with white
    if (image.hasAlphaChannel()) {
        QSize size = image.size();
        if (size.isEmpty()) {
            qWarning() << "Got non-null image with invalid size!";
            return 0;
        }

        QImage src = image;

        image = QImage(image.size(), QImage::Format_RGB32);

        // Filling with 50% gray, or average color from input, seems to not work
        image.fill(Qt::white);

        QPainter p(&image);
        p.drawImage(image.rect(), src);
#ifdef DEBUG_DUPES
        image.save("/tmp/postalpha-" + filename + ".png");
#endif
    }

    mean(image);
#ifdef DEBUG_DUPES
    image.save("/tmp/mean-" + filename + ".png");
#endif

    // threshold
    image = image.convertToFormat(QImage::Format_Mono, Qt::ThresholdDither);

#ifdef DEBUG_DUPES
    image.save("/tmp/mono-" + filename + ".png");
#endif

    image = image.scaled(9, 9, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

#ifdef DEBUG_DUPES
    image.save("/tmp/mini-" + filename + ".png");
#endif

    // scaling it automatically converts the format...
    image = image.convertToFormat(QImage::Format_Grayscale8);

#ifdef DEBUG_DUPES
    QImage diff(8, 8, QImage::Format_Grayscale8);
    diff.fill(Qt::black);
#endif
    std::bitset<64> bitset;
    for (int y=0; y<8; y++) {
        const uchar *line = image.scanLine(y);
#ifdef DEBUG_DUPES
        uchar *out = diff.scanLine(y);
#endif
        for (int x=0; x<8; x++) {
            bitset.set(y * 8 + x, line[x] > line[x+1]);
#ifdef DEBUG_DUPES
            out[x] = line[x] > line[x+1] ? 255 : 0;
#endif
        }
    }

#ifdef DEBUG_DUPES
    diff.save("/tmp/diff-" + filename + ".png");
#endif

    if (qEnvironmentVariableIsSet("PHOTOTONIC_BENCH") && t.elapsed()) {
        qDebug() << "Hashed" << filename << "in" << t.elapsed() << "ms";
    }

    return bitset.to_ullong();
}

Histogram ImageAnalysis::calcHist(const QImage &img)
{
    Histogram hist;
    if (img.isNull()) {
        return hist;
    }
    QImage image = img;

    if (image.width() != ANALYZE_SIZE || image.height() != ANALYZE_SIZE) {
        image = image.scaled(ANALYZE_SIZE, ANALYZE_SIZE, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    }
    if (!hist.load(image)) {
        qWarning() << "Failed to load";
        return hist;
    }
    hist.downsample();

    return hist;
}

void ImageAnalysis::analyzeImage(const QString &filename, const QImage &inputImage)
{
    if (loadedSortedFiles.contains(filename)) {
        return;
    }
    if (inputImage.isNull()) {
        Settings::failedFiles.insert(filename);
        return;
    }
    Settings::failedFiles.remove(filename);

    QImage image = inputImage;
    if (image.colorSpace() != QColorSpace::SRgb) {
        image.convertToColorSpace(QColorSpace::SRgb);
    }

    if (inputImage.width() != ANALYZE_SIZE || inputImage.height() != ANALYZE_SIZE) {
        image = inputImage.scaled(ANALYZE_SIZE, ANALYZE_SIZE, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    }

    switch(inputImage.format()) {
    case QImage::Format_ARGB32:
    case QImage::Format_RGB32:
        break;
    case QImage::Format_ARGB32_Premultiplied:
    default:
        image = image.convertToFormat(image.hasAlphaChannel() ? QImage::Format_ARGB32 : QImage::Format_RGB32);
        break;
    }

    imageHashes.append(calcImageHash2<32>(image, filename));
    //imageHashes.append(ImageAnalysis::calcImageHash2(image, filename));
    histograms.append(ImageAnalysis::calcHist(image));
    sortedFiles.append(filename);
    loadedSortedFiles.insert(filename);

}

void ImageAnalysis::sortBySimilarity(const bool withHash, QProgressDialog &progress)
{
    int processed = 0;

    const int rootIndex = withHash ? rootHashIndex : rootHistIndex;
    if (rootIndex >= 0 && rootIndex < sortedFiles.size()) {
        std::swap(sortedFiles[0], sortedFiles[rootIndex]);
        std::swap(histograms[0], histograms[rootIndex]);
        std::swap(imageHashes[0], imageHashes[rootIndex]);
    }

    for (int i=0; i<sortedFiles.size() - 1; i++) {
        float minScore = std::numeric_limits<float>::max();
        int minIndex = i+1;

        if (!withHash) {
            for (int j=i+1; j<sortedFiles.size(); j++) {
                const float score = histograms[i].compare(histograms[j]);
                if (score > minScore) {
                    continue;
                }
                minIndex = j;
                minScore = score;
            }
        } else {
            for (int j=i+1; j<sortedFiles.size(); j++) {
                const float score = histograms[i].compare(histograms[j]) *
                    POPCNT(imageHashes[i]  ^ imageHashes[j]);
                if (score > minScore) {
                    continue;
                }
                minIndex = j;
                minScore = score;
            }
        }

        processed += sortedFiles.size() - (i + 1);

        std::swap(sortedFiles[i+1], sortedFiles[minIndex]);
        std::swap(histograms[i+1], histograms[minIndex]);
        std::swap(imageHashes[i+1], imageHashes[minIndex]);

        if (processed > BATCH_SIZE * 10) {
            processed = 0;
            progress.show();
            progress.setValue(i);
            QCoreApplication::processEvents();
            if (progress.wasCanceled()) {
                return;
            }
        }
    }

}

void ImageAnalysis::selectRootIndices(const QStringList &selected)
{
    // if it is empty the loop will never run
    for (const QString &file : selected) {
        if (!loadedSortedFiles.contains(file) || Settings::failedFiles.contains(file)) {
            // shouldn't be possible, but whaterver
            continue;
        }

        rootHashIndex = sortedFiles.indexOf(file);

        if (rootHashIndex == -1) {
            // shouldn't be possible either
            continue;
        }

        rootHash = imageHashes[rootHashIndex];

        rootHistIndex = rootHashIndex;
        rootHist = histograms[rootHistIndex];
        break;
    }
    if (rootHashIndex == -1) {
        qWarning() << "Failed to find selected items, finding a good one";

        QElapsedTimer t; t.start();
        int bestBitCount = 64;

        rootHashIndex = 0;
        for (int i=0; i<imageHashes.count(); i++) {
            const uint64_t bits = imageHashes[i];
            const int distance = qAbs(POPCNT(bits) - 32);//qAbs(bits.count(true) - 32);
            if (distance < bestBitCount) {
                rootHash = bits;
                bestBitCount = distance;
                rootHashIndex = i;
            }
            if (distance >= 31) {
                break;
            }
        }
    }

    if (rootHistIndex == -1) {
        float bestScore = std::numeric_limits<float>::max();
        Histogram empty;
        for (int i=0; i<256; i++) {
            empty.red[i] = 1.;
            empty.green[i] = 1.;
            empty.blue[i] = 1.;
        }

        rootHistIndex = 0;
        for (int i=0; i<histograms.size(); i++) {
            const Histogram &histogram = histograms[i];
            const float score = histogram.compare(empty) +
                    histogram.compareChannel(histogram.blue, histogram.red) +
                    histogram.compareChannel(histogram.green, histogram.red) +
                    histogram.compareChannel(histogram.blue, histogram.green);
            if (std::isnan(score)) {
                continue;
            }

            if (score > bestScore) {
                continue;
            }

            rootHistIndex = i;
            rootHist = histogram;
            bestScore = score;
        }
    }
}
