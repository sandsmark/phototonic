#pragma once

#include <QImageIOHandler>

bool losslessTransformJPEG(const QString &filename, const QImageIOHandler::Transformation transform, const QRect &crop, QString *error, bool perfect = true);

