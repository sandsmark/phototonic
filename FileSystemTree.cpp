/*
 *  Copyright (C) 2013-2014 Ofer Kashayov <oferkv@live.com>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FileSystemTree.h"
#include "FileSystemModel.h"

#include <QAction>
#include <QScrollBar>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QMimeData>

FileSystemTree::FileSystemTree(QWidget *parent) : QTreeView(parent) {
    setAcceptDrops(true);
    setDragEnabled(true);
    setAnimated(false);
    setDragDropMode(QAbstractItemView::InternalMove);

    fileSystemModel = new FileSystemModel(this);
    fileSystemModel->setRootPath("");
    setModelFlags();
    setModel(fileSystemModel);

    for (int i = 1; i <= 3; ++i) {
        hideColumn(i);
    }
    setHeaderHidden(true);

    // Work around issues with QFileSystemModel lazy-loading
    connect(fileSystemModel, &QFileSystemModel::layoutChanged, this, [this]() {
            if (!m_forceScrollTo) {
                return;
            }
            m_forceScrollTo = false;
            const QRect currentRect = visualRect(currentIndex());
            if (viewport()->rect().intersects(currentRect)) {
                return;
            }
            resizeTreeColumn(currentIndex());
            scrollTo(currentIndex(), QAbstractItemView::PositionAtCenter);
            horizontalScrollBar()->setValue(horizontalScrollBar()->value() + currentRect.x() - indentation());
        }, Qt::QueuedConnection);

    connect(this, SIGNAL(expanded(
                                 const QModelIndex &)),
            this, SLOT(resizeTreeColumn(
                               const QModelIndex &)));
    connect(this, SIGNAL(collapsed(
                                 const QModelIndex &)),
            this, SLOT(resizeTreeColumn(
                               const QModelIndex &)));

    QAction *showHiddenAction = new QAction("Show hidden directories", this);
    showHiddenAction->setCheckable(true);
    showHiddenAction->setChecked(Settings::showHiddenDirs);
    connect(showHiddenAction, &QAction::toggled, this, &FileSystemTree::onToggleShowHidden);
    addAction(showHiddenAction);
}

// Borrowed from qtc
void FileSystemTree::scrollTo(const QModelIndex &index, QAbstractItemView::ScrollHint hint)
{
    // work around QTBUG-3927
    QScrollBar *hBar = horizontalScrollBar();
    int scrollX = hBar->value();
    const int viewportWidth = viewport()->width();
    QRect itemRect = visualRect(index);
    QAbstractItemDelegate *delegate = itemDelegate(index);
    if (delegate) {
        itemRect.setWidth(delegate->sizeHint(viewOptions(), index).width());
    }

    if (itemRect.x() - indentation() < 0) {
        // scroll so left edge minus one indent of item is visible
        scrollX += itemRect.x() - indentation();
    } else if (itemRect.right() > viewportWidth) {
        // If right edge of item is not visible and left edge is "too far right",
        // then move so it is either fully visible, or to the left edge.
        // For this move the left edge one indent to the left, so the parent can potentially
        // still be visible.
        if (itemRect.width() + indentation() < viewportWidth)
            scrollX += itemRect.right() - viewportWidth;
        else
            scrollX += itemRect.x() - indentation();
    }
    scrollX = qBound(hBar->minimum(), scrollX, hBar->maximum());
    QTreeView::scrollTo(index, hint);
    hBar->setValue(scrollX);
}


QModelIndex FileSystemTree::getCurrentIndex() {
    return selectedIndexes().first();
}

void FileSystemTree::setCurrentPath(const QString &path) {
    if (path.isEmpty()) {
        return;
    }
    if (!QFileInfo(path).isAbsolute()) {
        return;
    }
    QModelIndex idx = fileSystemModel->index(path);
    if (!idx.isValid()) {
        return;
    }
    m_forceScrollTo = true;
    expand(idx);
    setCurrentIndex(idx);
}

void FileSystemTree::resizeTreeColumn(const QModelIndex &index) {
    resizeColumnToContents(0);
    scrollTo(index);
}

void FileSystemTree::dragEnterEvent(QDragEnterEvent *event) {
    QModelIndexList selectedDirs = selectionModel()->selectedRows();
    if (selectedDirs.size() > 0) {
        dndOrigSelection = selectedDirs[0];
        event->acceptProposedAction();
    }
}

void FileSystemTree::dragMoveEvent(QDragMoveEvent *event) {
    setCurrentIndex(indexAt(event->pos()));
}

void FileSystemTree::dropEvent(QDropEvent *event) {
    if (event->source()) {
        QString fileSystemTreeStr = "FileSystemTree";
        bool dirOp = (event->source()->metaObject()->className() == fileSystemTreeStr);
        emit dropOp(event->keyboardModifiers(), dirOp, event->mimeData()->urls().at(0).toLocalFile());
        setCurrentIndex(dndOrigSelection);
    }
}

void FileSystemTree::onToggleShowHidden(bool showHidden)
{
    Settings::showHiddenDirs = showHidden;
    if (Settings::showHiddenDirs) {
        fileSystemModel->setFilter(fileSystemModel->filter() | QDir::Hidden);
    } else {
        fileSystemModel->setFilter(fileSystemModel->filter() ^ QDir::Hidden);
    }
}

void FileSystemTree::setModelFlags() {
    fileSystemModel->setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);
    if (Settings::showHiddenDirs) {
        fileSystemModel->setFilter(fileSystemModel->filter() | QDir::Hidden);
    }
}

