/*
 *  Copyright (C) 2013-2014 Ofer Kashayov <oferkv@live.com>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Bookmarks.h"

#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QMimeData>

BookMarks::BookMarks(QWidget *parent) : QListWidget(parent) {
    setAcceptDrops(true);
    setDragEnabled(false);
    setDragDropMode(QAbstractItemView::DropOnly);

    reloadBookmarks();
}

void BookMarks::reloadBookmarks() {
    clear();
    for (const QString &itemPath : Settings::bookmarkPaths) {
        QListWidgetItem *item = new QListWidgetItem(this);
        item->setText(QFileInfo(itemPath).fileName());
        item->setIcon(QIcon(":/images/bookmarks.png"));
        item->setToolTip(itemPath);

        if (!QFileInfo(itemPath).isDir()) {
            item->setFlags(item->flags() ^ Qt::ItemIsEnabled);
        }
        addItem(item);
    }
}

void BookMarks::removeBookmark() {
    if (selectedItems().size() == 1) {
        Settings::bookmarkPaths.removeAll(selectedItems().at(0)->toolTip());
        reloadBookmarks();
    }
}

void BookMarks::dragEnterEvent(QDragEnterEvent *event) {
    QModelIndexList selectedDirs = selectionModel()->selectedRows();

    if (selectedDirs.size() > 0) {
        dndOrigSelection = selectedDirs[0];
    }
    event->acceptProposedAction();
}

void BookMarks::dragMoveEvent(QDragMoveEvent *event) {
    setCurrentIndex(indexAt(event->pos()));
}

void BookMarks::dropEvent(QDropEvent *event) {
    if (event->source()) {
        QString fileSystemTreeStr("FileSystemTree");
        bool dirOp = (event->source()->metaObject()->className() == fileSystemTreeStr);
        emit dropOp(event->keyboardModifiers(), dirOp, event->mimeData()->urls().at(0).toLocalFile());
    }
}

