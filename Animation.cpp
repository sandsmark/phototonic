/*
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "Animation.h"
#include "Settings.h"

Animation::Animation(QObject *parent) : QObject(parent)
{
    m_timer.setSingleShot(true);
    connect(&m_timer, &QTimer::timeout, this, &Animation::onTimeout);
}

int Animation::frameCount() {
    if (m_cacheFull) {
        return imageCache.count();
    }
    if (m_brokenFile) {
        return m_largestImageNumber;
    }
    int count = reader.imageCount();
    if (count <= 0) {
        return m_largestImageNumber;
    }
    return count;
}

const QImage &Animation::load(QIODevice *device) {
    m_timer.stop();
    state = Paused;

    if (reader.device()) {
        reader.device()->deleteLater();
    }

    m_cacheFull = false;
    m_brokenFile = false;
    reader.setDevice(device);
    reader.setAutoTransform(Settings::exifRotationEnabled);
    currentImage = QImage();
    imageCache.clear();
    m_delayCache.clear();

    m_currentFrame = 0;
    m_largestImageNumber = 0;
    m_timeLeft = 0;

    if (device) {
        currentImage = reader.read();
        m_timeLeft = reader.nextImageDelay();
    }

    return currentImage;
}
void Animation::jumpToImage(const int num) {
    if (num == m_currentFrame + 1) {
        m_timer.stop();
        if (!readNextImage(false)) {
            handleReaderError(false);
        }
        return;
    }
    m_currentFrame = num;
    if (cacheFrames) {
        if (m_cacheFull) {
            if (m_currentFrame >= imageCache.size()) {
                m_currentFrame = imageCache.size() - 1;
            }
        }
        if (m_currentFrame < imageCache.size()) {
            currentImage = imageCache[m_currentFrame];
            emit nextFrameReady(currentImage);
            return;
        }
    }
    if (!reader.jumpToImage(num)) {
        qWarning() << "Failed to jump to" << num;
    }
}
void Animation::start() {
    state = Playing;
    m_timer.start(qMax(m_timeLeft, 0));
    m_timeLeft = 0;
}

void Animation::pause() {
    state = Paused;
    m_timeLeft = m_timer.remainingTime();
    m_timer.stop();
}

bool Animation::readNextImage(bool startTimer) {
    if (currentImage.isNull()) {
        if (m_currentFrame != 0) {
            qWarning() << "Invalid state";
            return false;
        }
        currentImage = reader.read();
        if (currentImage.isNull()) {
            qWarning() << "Failed to read first frame" << reader.errorString();
            emit error(reader.errorString());
            return false;
        }
    }
    QElapsedTimer t; t.start();

    emit nextFrameReady(std::move(currentImage));

    currentImage = QImage();

    if (cacheFrames) {
        if (m_cacheFull && m_currentFrame >= imageCache.size()) {
            m_currentFrame = 0;
        }
        if (m_currentFrame < imageCache.size()) {
            Q_ASSERT(imageCache.size() == m_delayCache.size());
            if (!imageCache[m_currentFrame].isNull()) {
                currentImage = imageCache[m_currentFrame];
                setNextTimer(m_delayCache[m_currentFrame], t.elapsed());
                m_currentFrame++;
                return true;
            } else {
                qWarning() << "Null in cache!";
            }
        }
    }

    if (m_cacheFull) {
        qWarning() << "We shouldn't get here";
    }

    if (reader.canRead()) {
        currentImage = reader.read();
    }

    if (currentImage.isNull()) {
        return false;
    }

    int readerImageNumber = reader.currentImageNumber();
    if (readerImageNumber < 0) {
        readerImageNumber = m_currentFrame + 1;
    }

    const int timeToNext = qMax(reader.nextImageDelay(), 10);

    if (cacheFrames) {
        if (m_currentFrame >= imageCache.size()) {
            imageCache.resize(m_currentFrame + 1);
        }
        imageCache[m_currentFrame] = currentImage;

        if (m_currentFrame >= m_delayCache.size()) {
            m_delayCache.resize(m_currentFrame + 1);
        }
        m_delayCache[m_currentFrame] = timeToNext;
    }

    if (startTimer) {
        setNextTimer(timeToNext, t.elapsed());
    }

    int count = reader.imageCount();
    if ((count >= 0 && readerImageNumber >= count) || readerImageNumber < m_currentFrame) {
        if (cacheFrames) {
            m_cacheFull = true;
        } else {
            if (!reader.jumpToImage(0)) {
                qWarning() << "Failed to seek to start";
                return false;
            }
            m_currentFrame = 0;
        }
    } else {
        m_cacheFull = false;
        m_currentFrame++;
    }
    m_largestImageNumber = qMax(m_currentFrame, m_largestImageNumber);
    return true;
}

void Animation::handleReaderError(bool startTimer) {
    QElapsedTimer t; t.start();
    if (!cacheFrames || imageCache.isEmpty() || imageCache.first().isNull()) {
        if (m_currentFrame <= 0) {
            emit error(reader.errorString());
            return;
        }

        qDebug() << "Cache not enabled, trying to reset";
        m_brokenFile = true;
        m_currentFrame = 0;
        QIODevice *device = reader.device();
        reader.setDevice(nullptr);
        device->seek(0);
        reader.setDevice(device);

        if (!readNextImage(startTimer)) {
            qWarning() << "Really failed to read";
            QImage image = currentImage;
            QString errorMessage = reader.errorString();
            load(nullptr);
            currentImage = image;
            emit error(errorMessage);
        }
        return;
    }

    m_cacheFull = true;
    m_currentFrame = 0;
    currentImage = imageCache[m_currentFrame];
    if (startTimer) {
        setNextTimer(m_delayCache[m_currentFrame], t.elapsed());
    }
}

void Animation::setNextTimer(int delay, int processingTime) {
    delay -= processingTime;
    speed = qBound(1, speed, 1000);
    delay = 100 * delay / speed;
    m_timer.start(qMax(delay, 0));
}
