#pragma once

#include <QImage>
#include <QSet>

struct ThumbDiskCache
{
    static bool isSlow(const QString &format) {
        static const QSet<QString> blacklistedDecoders = {
            // Certain image format plugins decode the entire image when queried for
            // the size...
            "heif",
            "svg",
            "pdf",
            // and some are just plain old slow
            "avif",
            "avis",
        };
        return blacklistedDecoders.contains(format);

    }
    static QImage loadThumbImage(const QString &filename, const int targetSize, const bool shrinkable);

    static QString thumbnailFileName(const QString &path);
    static QString locateThumbnail(const QString &path, const QSize &targetSize, const bool shrinkable);
    static void storeThumbnail(const QString &originalPath, QImage thumbnail, const QSize &originalSize);

    static bool getOriginalMetadata(const QString &originalPath, QSize *dimensions, bool deleteFailed);
};

