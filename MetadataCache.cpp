/*
 *  Copyright (C) 2013-2015 Ofer Kashayov <oferkv@live.com>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Exiv.h"
#include "Settings.h"
#include "MetadataCache.h"
#include "ThumbDiskCache.h"

#include <QImageReader>
#include <QElapsedTimer>

static const bool s_benchmark = qEnvironmentVariableIsSet("PHOTOTONIC_BENCHMARK");

void MetadataCache::updateImageTags(const QString &imageFileName, QSet<QString> tags) {
    cache[imageFileName].tags = tags;
}

bool MetadataCache::removeTagFromImage(const QString &imageFileName, const QString &tagName) {
    return cache[imageFileName].tags.remove(tagName);
}

void MetadataCache::removeImage(const QString &imageFileName) {
    cache.remove(imageFileName);
}

QSet<QString> &MetadataCache::getImageTags(const QString &imageFileName) {
    return cache[imageFileName].tags;
}

QImageIOHandler::Transformations MetadataCache::getImageOrientation(const QString &imageFileName) {
    QElapsedTimer t; t.start();

    // Lazily load this, because it can be slow
    ImageMetadata &metadata = cache[imageFileName];
    if (metadata.readTransformations) {
        return metadata.transformations;
    }

    if (metadata.lastModified == 0) { // It has not been put into the cache yet, so load it properly
        if (loadImageMetadata(imageFileName) && metadata.readTransformations) {
            if (s_benchmark && t.elapsed()) qDebug() << "load transform from metadata" << t.elapsed();
            return metadata.transformations;
        }
    }

    QImageReader reader(imageFileName);
    metadata.transformations = reader.transformation();
    metadata.readTransformations = true;
    if (s_benchmark && t.elapsed()) qDebug() << "Manual image transform read" << t.elapsed();
    return metadata.transformations;
}

QSize MetadataCache::getResolution(const QString &imageFileName) {
    // More lazy load, because it can fall back on reading the entire image (via the thumbnails)

    QElapsedTimer t; t.start();

    ImageMetadata &metadata = cache[imageFileName];
    if (metadata.readResolution) {
        return metadata.resolution;
    }
    if (loadImageMetadata(imageFileName) && metadata.readResolution) {
        if (s_benchmark && t.elapsed()) qDebug() << "load size from metadata" << t.elapsed();
        return metadata.resolution;
    }
    metadata.readResolution = true;

    if (ThumbDiskCache::getOriginalMetadata(imageFileName,&metadata.resolution, true)) {
        if (s_benchmark && t.elapsed()) qDebug() << "load thumbnail metadata" << t.elapsed();
        return metadata.resolution;
    }

    QImageReader reader(imageFileName);
    metadata.resolution = reader.size();
    if (!metadata.resolution.isEmpty()) {
        if (s_benchmark && t.elapsed()) qDebug() << "manual read size only" << t.elapsed();
        return metadata.resolution;
    }

    // Try to force creation of thumbnail, which will read the image
    if (!ThumbDiskCache::loadThumbImage(imageFileName, ANALYZE_SIZE, false).isNull()) {
        if (!ThumbDiskCache::getOriginalMetadata(imageFileName, &metadata.resolution, true)) {
            qWarning() << "Thumbnail generation succeeded but stored size was empty?" << imageFileName;
        }
    }
    if (s_benchmark && t.elapsed()) qDebug() << "create thumbnail metadata" << t.elapsed();

    return metadata.resolution;
}

void MetadataCache::setImageTags(const QString &imageFileName, QSet<QString> tags) {
    ImageMetadata imageMetadata;

    imageMetadata.tags = tags;
    cache.insert(imageFileName, imageMetadata);
}

void MetadataCache::addTagToImage(const QString &imageFileName, QString &tagName) {
    if (cache[imageFileName].tags.contains(tagName)) {
        return;
    }

    cache[imageFileName].tags.insert(tagName);
}

void MetadataCache::clear() {
    cache.clear();
}

static QImageIOHandler::Transformations exif2Qt(int exifOrientation)
{
    switch (exifOrientation) {
    case 0: // unspecified
        return QImageIOHandler::TransformationNone;
    case 1: // normal
        return QImageIOHandler::TransformationNone;
    case 2: // mirror horizontal
        return QImageIOHandler::TransformationMirror;
    case 3: // rotate 180
        return QImageIOHandler::TransformationRotate180;
    case 4: // mirror vertical
        return QImageIOHandler::TransformationFlip;
    case 5: // mirror horizontal and rotate 270 CW
        return QImageIOHandler::TransformationFlipAndRotate90;
    case 6: // rotate 90 CW
        return QImageIOHandler::TransformationRotate90;
    case 7: // mirror horizontal and rotate 90 CW
        return QImageIOHandler::TransformationMirrorAndRotate90;
    case 8: // rotate 270 CW
        return QImageIOHandler::TransformationRotate270;
    }
    qWarning() << "Invalid EXIF orientation" << exifOrientation;
    return QImageIOHandler::TransformationNone;
}

bool MetadataCache::loadImageMetadata(const QString &imageFullPath) {
    ImageMetadata &imageMetadata = cache[imageFullPath];
    const qint64 lastModified = QFileInfo(imageFullPath).lastModified().toMSecsSinceEpoch();
    if (imageMetadata.lastModified == lastModified) {
        return imageMetadata.good;
    }
    imageMetadata.lastModified = lastModified;

    IGNORE_EXIV2_DEPRECATED_START;
    Exiv2::Image::AutoPtr exifImage;
    IGNORE_EXIV2_DEPRECATED_END;

    QSet<QString> tags;


    try {
        exifImage = Exiv2::ImageFactory::open(imageFullPath.toStdString());
        exifImage->readMetadata();
    } catch (Exiv2::Error &error) {
        if (error.code() != Exiv2::kerFileContainsUnknownImageType) {
            qWarning() << "exiv2: Error loading image for reading metadata" << error.what() << error.code() << imageFullPath;
        }
        return false;
    }

    if (!exifImage->good()) {
        qWarning() << "Bad exif image" << imageFullPath;
        return false;
    }

    if (exifImage->supportsMetadata(Exiv2::mdExif)) try {
        Exiv2::ExifData::const_iterator it = Exiv2::orientation(exifImage->exifData());
        if (it != exifImage->exifData().end()) {
            imageMetadata.transformations = exif2Qt(it->toLong());
            imageMetadata.readTransformations = true;
        }
    } catch (Exiv2::Error &error) {
        qWarning() << "exiv2: Failed to read Exif metadata" << error.what();
    }

    if (exifImage->supportsMetadata(Exiv2::mdIptc)) try {
        Exiv2::IptcData &iptcData = exifImage->iptcData();
        if (!iptcData.empty()) {
            QString key;
            Exiv2::IptcData::iterator end = iptcData.end();

            // Finds the first ID, but we need to loop over the rest in case there are more
            Exiv2::IptcData::iterator iptcIt = iptcData.findId(Exiv2::IptcDataSets::Keywords);
            for (; iptcIt != end; ++iptcIt) {
                if (iptcIt->tag() != Exiv2::IptcDataSets::Keywords) {
                    continue;
                }

                QString tagName = QString::fromUtf8(iptcIt->toString().c_str());
                tags.insert(tagName);
                Settings::knownTags.insert(tagName);
            }
        }
    } catch (Exiv2::Error &error) {
        qWarning() << "exiv2: Failed to read Iptc metadata";
    }

    if (tags.size()) {
        imageMetadata.tags = tags;
    }
    imageMetadata.resolution = QSize(exifImage->pixelWidth(), exifImage->pixelHeight());

    if (imageMetadata.resolution.isEmpty()) {
        if (ThumbDiskCache::getOriginalMetadata(imageFullPath,&imageMetadata.resolution, false)) {
            imageMetadata.readResolution = true;
        }
    } else {
        imageMetadata.readResolution = true;
    }

    // The isSlow is a manual heuristic for certain broken image plugins that
    // load the entire image when queried for size (e. g. several in
    // kimageformats)
    if (imageMetadata.resolution.isEmpty()) {
        QImageReader reader(imageFullPath);
        if (!ThumbDiskCache::isSlow(reader.format())) {
            imageMetadata.resolution = reader.size();
        }
    }
    imageMetadata.readResolution = !imageMetadata.resolution.isEmpty();

    imageMetadata.good = true;

    return true;
}

