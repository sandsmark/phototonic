#include "DirLister.h"
#include "Settings.h"

#include <QImageReader>
#include <QFile>
#include <QMimeDatabase>

FormatProber::FormatProber() {
    const QList<QByteArray> supportedFormats = QImageReader::supportedImageFormats();
    if (supportedFormats.contains("webp")) {
        webpSupported = true;
    }
    if (supportedFormats.contains("avif")) {
        avifSupported = true;
    }
    if (supportedFormats.contains("heic")) {
        heicSupported = true;
    }
    if (supportedFormats.contains("ico")) {
        icoSupported = true;
    }

    if (supportedFormats.contains("webm")) {
        webmSupported = true;
        mp4Supported = true; // just an assumption for now, since qt-ffmpeg-imageplugin doesn't register mp4 as supported to avoid mucking up things
        matroskaSupported = true;
    }
    QMimeDatabase db;
    for (const QByteArray &mimeName : QImageReader::supportedMimeTypes()) {
        const QMimeType mimetype = db.mimeTypeForName(QString::fromLocal8Bit(mimeName));
        if (mimetype.isDefault() || !mimetype.isValid()) {
            qWarning() << "Invalid mime name" << mimeName;
            continue;
        }
        supportedMimetypes.insert(mimetype.name());
        for (const QString &ancestor : mimetype.allAncestors()) {
            if (ancestor == "application/octet-stream") {
                continue;
            }
            if (!ancestor.startsWith("image") && !ancestor.startsWith("video")) {
                continue;
            }
            supportedMimetypes.insert(ancestor);
        }
    }
}

static bool isMatroska(const QByteArray &buffer) {
    if (buffer.size() < 79) {
        return false;
    }

    static const char *ebmlID = "\x1a\x45\xdf\xa3";
    if (!buffer.startsWith(ebmlID)) {
        return false;
    }

    static const char *docID = "\x42\x82";
    int index = buffer.indexOf(docID);
    if (index < 5 || index > 65) {
        return false;
    }

    //static const char *webmID = "webm";
    //index = buffer.indexOf(webmID);
    //if (index >= 8 && index <= 75) {
    //    return true;
    //}
    static const char *matroskaID = "matroska";
    index = buffer.indexOf(matroskaID);
    if (index >= 8 && index <= 75) {
        return true;
    }

    return false;
}
#if defined(STARTS_WITH) || defined(STARTS_WITH_AT)
#error "define collision"
#endif
#define STARTS_WITH(x) (memcmp(rawBuffer, x, sizeof(x) - 1) == 0)
#define STARTS_WITH_AT(offset, x) (memcmp(rawBuffer + offset, x, sizeof(x) - 1) == 0)

FormatProber::ImageType FormatProber::probe(const QString &filePath) const {
    QFile f(filePath);
    if (!f.open(QIODevice::ReadOnly)) {
        return NotImage;
    }
    const QByteArray buffer = f.read(probeSize);
    if (buffer.length() < probeSize) {
        return NotImage;
    }
    const char *rawBuffer = buffer.constData();

    // png
    static constexpr const char pngHeader[] = "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A";
    static_assert(probeSize >= sizeof(pngHeader));
    if (STARTS_WITH(pngHeader)) {
        return Png;
    }
    // jpeg
    static constexpr const char jpegHeader[] = "\xff\xd8\xff";
    static_assert(probeSize >= sizeof(jpegHeader));
    if (STARTS_WITH(jpegHeader)) {
        return Jpeg;
    }

    // gif
    static constexpr const char gifHeader[] = "GIF8";
    static_assert(probeSize >= sizeof(gifHeader));
    if (STARTS_WITH(gifHeader)) {
        return Gif;
    }

    //webp
    static constexpr const char webpMagic[] = "WEBP";
    static_assert(probeSize >= sizeof(webpMagic) + 8);
    if (webpSupported && STARTS_WITH("RIFF") && memcmp(rawBuffer + 8, webpMagic, sizeof(webpMagic) - 1) == 0) {
        return WebP;
    }

    static constexpr const char tripleNull[] = "\0\0\0";
    static constexpr const char ftypMagic[] = "ftyp";
    static_assert(probeSize >= sizeof(tripleNull) + sizeof(ftypMagic) + 4);
    if ((heicSupported || mp4Supported) && STARTS_WITH(tripleNull) && STARTS_WITH_AT(4, ftypMagic)) {
        if (heicSupported) {
            static constexpr const char heicMagic[] = "hei";
            if (STARTS_WITH_AT(8, heicMagic)) {
                return Heic;
            }
        }
        if (mp4Supported) {
            static constexpr const char isoMagic[] = "iso";
            if (STARTS_WITH_AT(8, isoMagic)) {
                return Mp4;
            }

            static constexpr const char mp4Magic[] = "mp4";
            if (STARTS_WITH_AT(8, mp4Magic)) {
                return Mp4;
            }

            static constexpr const char faceMagic[] = "FACE";
            if (STARTS_WITH_AT(8, faceMagic)) {
                return Mp4;
            }

            static constexpr const char quicktimeMagic[] = "qt  ";
            if (STARTS_WITH_AT(8, quicktimeMagic)) {
                return Mp4;
            }

            static constexpr const char m4aMagic[] = "M4A";
            if (STARTS_WITH_AT(8, m4aMagic)) {
                return NotImage;
            }

            static constexpr const char threeGpMagic[] = "3gp";
            if (STARTS_WITH_AT(8, threeGpMagic)) {
                return Mp4;
            }

            static constexpr const char avisMagic[] = "avis";
            if (STARTS_WITH_AT(8, avisMagic)) {
                return Avis;
            }
        }
        if (avifSupported) {
            static constexpr const char avifMagic[] = "avif";
            if (STARTS_WITH_AT(8, avifMagic)) {
                return Avif;
            }
        }
    }

    // Matroska
    if (matroskaSupported && isMatroska(buffer)) {
        return Matroska;
    }

    // bmp
    if (STARTS_WITH("BM")) {
        uint32_t sig;
        memcpy(&sig, rawBuffer + 14, sizeof sig);

        switch(sig) {
        case 16:
            qDebug() << "PC bitmap, OS/2 2.x format (DIB header size=16)";
            return Bmp;
        case 24:
            qDebug() << "PC bitmap, OS/2 2.x format (DIB header size=24)";
            return Bmp;
        case 48:
            qDebug() << "PC bitmap, OS/2 2.x format (DIB header size=48)";
            return Bmp;
        case 64:
            qDebug() << "PC bitmap, OS/2 2.x format";
            return Bmp;
        case 40:
            qDebug() << "PC bitmap, Windows 3.x format";
            return Bmp;
        case 108:
            qDebug() << "PC bitmap, Windows 95/NT4 and newer format";
            return Bmp;
        case 124:
            qDebug() << "PC bitmap, Windows 98/2000 and newer format";
            return Bmp;
        case 52:
            qDebug() << "PC bitmap, Adobe Photoshop";
            return Bmp;
        case 56:
            qDebug() << "PC bitmap, Adobe Photoshop with alpha channel mask";
            return Bmp;

            // There are some more obscure ones, but I'm lazy
        default:
            break;
        }
    }
    // ico
    // offset 4 is number of frames
    static_assert(probeSize > 8);
    if (icoSupported && STARTS_WITH("\x0\x0\x1\x0") && rawBuffer[4] != 0) {
        return Ico;
    }
    // dib
    static_assert(probeSize > 12);
    if (rawBuffer[0] == 40 && rawBuffer[12] == 1) {
        return Dib;
    }

    const QMimeDatabase db; // the class is extremely light, it has only a pointer to a shared instance
    f.seek(0);
    const QString mimetype = db.mimeTypeForFileNameAndData(filePath, &f).name();
    static const QSet<QString> xdgMimeBlacklist = { // known broken matches in the xdg mime database
        "image/x-tga"
    };
    if (xdgMimeBlacklist.contains(mimetype)) {
        if (db.mimeTypeForName(filePath).name() != mimetype) {
            return NotImage;
        }
    }
    if (supportedMimetypes.contains(mimetype)) {
        return OtherImageFormat;
    }

    return NotImage;
}
#undef STARTS_WITH
#undef STARTS_WITH_AT

#if 0
bool FormatProber::isWebPLossless(const QString &filePath) const
{
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << file.errorString();
        return false;
    }
    WebPDecoderConfig config;
    if (!WebPInitDecoderConfig(&config)) {
        qWarning() << "failed to init webp config";
        return false;
    }

    // horribly inefficient, but a hassle to do more
    const QByteArray contents = file.readAll();
    const VP8StatusCode status = WebPGetFeatures((const uint8_t*)contents.data(), contents.size(), &config.input);
    if (status != VP8_STATUS_OK) {
        if (status == VP8_STATUS_BITSTREAM_ERROR) {
            qWarning() << "Invalid bitstream (not webp?)";
        } else {
            qWarning() << "Failed to read features" << int(status);
        }
        return false;
    }
    switch(config.input.format) {
    case 0: // mixed
        return false;
    case 1: // lossy
        return false;
    case 2: // lossless
        return true;
    default:
        qWarning() << "Unknown webp format" << config.input.format;
        return false;
    }
}
#endif

QFileInfoList DirLister::entryInfoList() {
    if (!Settings::probeFiles && filterString.isEmpty()) {
        dir.setNameFilters(nameFilters);
        return dir.entryInfoList();
    }

    dir.setNameFilters(Settings::probeFiles ? QStringList() : nameFilters);
    QFileInfoList ret;
    for (const QFileInfo &info : dir.entryInfoList()) {
        if (!info.fileName().contains(filterString, Qt::CaseInsensitive)) { // empty filter string always matches
            continue;
        }

        if (suffixes.contains(info.suffix().toLower())) {
            ret.append(info);
            continue;
        }

        if (FormatProber::imageType(info.absoluteFilePath()) != FormatProber::NotImage) {
            ret.append(info);
        }
    }
    return ret;
}
