/*
 *  Copyright (C) 2018 Shawn Rutledge <s@ecloud.org>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ImageWidget.h"
#include "Animation.h"
#include "Settings.h"
#include <QDebug>
#include <QPainter>
#include <QPaintEvent>

ImageWidget::ImageWidget(QWidget *parent) : QWidget(parent)
{
    m_animation = new Animation(this);
    connect(m_animation, &Animation::nextFrameReady, this, &ImageWidget::onNextFrameReady);
}

bool ImageWidget::empty()
{
    return m_image.isNull();
}

void ImageWidget::onNextFrameReady(const QImage &frame)
{
    m_image = frame;
    update();
}

bool ImageWidget::animation() const
{
    return m_animation->isActive();
}

bool ImageWidget::loadAnimation(const QString &filename)
{
    m_image = m_animation->load(new QFile(filename));
    if (m_image.isNull()) {
        return false;
    }
    m_animation->start();
    return true;
}

QImage ImageWidget::image()
{
    if (m_image.isNull() && m_animation->isActive()) {
        const_cast<ImageWidget*>(this)->m_image = m_animation->lastImage();
    }
    return m_image;
}

void ImageWidget::resizeEvent(QResizeEvent *event)
{
    const qreal deltaX = event->size().width() - event->oldSize().width();
    const qreal deltaY = event->size().height() - event->oldSize().height();
    m_viewRect.translate(deltaX/2, deltaY/2);
}

void ImageWidget::setViewSize(const QSizeF viewSize, const QPointF &cursor)
{
    if (!m_viewRect.isValid()) {
        m_viewRect = QRect(0, 0, viewSize.width(), viewSize.height());
        m_viewRect.moveCenter(rect().center());
    } else {
        QPointF centerPoint;
        bool zoomingOut = viewSize.width() < m_viewRect.width() || viewSize.height() < m_viewRect.height();
        if (zoomingOut) {
            centerPoint = rect().center();
        } else {
            centerPoint = cursor;

            if (centerPoint.x() < m_viewRect.left()) {
                centerPoint.setX(m_viewRect.left());
            } else if (centerPoint.x() > m_viewRect.right()) {
                centerPoint.setX(m_viewRect.right());
            }
            if (centerPoint.y() < m_viewRect.top()) {
                centerPoint.setY(m_viewRect.top());
            } else if (centerPoint.y() > m_viewRect.bottom()) {
                centerPoint.setY(m_viewRect.bottom());
            }
        }
        if (viewSize.width() < width()) {
            centerPoint.setX(rect().center().x());
        }
        if (viewSize.height() < height()) {
            centerPoint.setY(rect().center().y());
        }
        QPointF delta(viewSize.width() * (m_viewRect.x()-centerPoint.x())/ m_viewRect.width()+centerPoint.x(), viewSize.height()*(m_viewRect.y()-centerPoint.y())/m_viewRect.height() + centerPoint.y());
        if (zoomingOut) {
            m_viewRect.moveTo(delta);
            m_viewRect.setSize(viewSize);
        } else {
            m_viewRect.setSize(viewSize);
            m_viewRect.moveTo(delta);
        }
    }

    if (!rect().isValid()) {
        return;
    }
    m_viewRect = constrainRect(m_viewRect);
    update();
}

void ImageWidget::setViewRect(const QRectF viewRect)
{
    m_viewRect = viewRect;
    update();
}

void ImageWidget::setImage(const QImage &i, bool refresh)
{
    m_animation->load(nullptr);

    m_image = i;

    if (!refresh) {
        m_viewRect = QRect();
        m_rotation = 0;
    }
    update();
}

void ImageWidget::setTempRotation(qreal r)
{
    m_rotation = r;
    update();
}

QTransform ImageWidget::imageMapTransform() const
{
    QTransform transform;

    qreal scaleX = m_viewRect.width() / m_image.width();
    qreal scaleY = m_viewRect.height() / m_image.height();
    transform.scale(1./scaleX, 1./scaleY);
    transform.translate(-m_viewRect.x(), -m_viewRect.y());
    return transform;
}

QPoint ImageWidget::mapToImage(QPoint p)
{
    return imageMapTransform().map(p);
}

QPoint ImageWidget::mapFromImage(QPoint p)
{
    return imageMapTransform().inverted().map(p);
}

QSize ImageWidget::imageSize() const
{
    if (m_image.isNull() && m_animation->isActive()) {
        const_cast<ImageWidget*>(this)->m_image = m_animation->lastImage();
    }
    return m_image.size();
}

QSize ImageWidget::sizeHint() const
{
    if (m_image.isNull() && m_animation->isActive()) {
        const_cast<ImageWidget*>(this)->m_image = m_animation->lastImage();
    }
    return m_image.size();
}

void ImageWidget::setCropPreview(const QRect &newRect)
{
    m_cropRect = newRect;
    update();
}

QRectF ImageWidget::constrainRect(QRectF viewRect) const
{
    if (viewRect.height() > height()) {
        if (viewRect.top() > 0) {
            viewRect.moveTop(0);
        } else if (viewRect.bottom() < height()) {
            viewRect.moveBottom(height());
        }
    } else {
        if (viewRect.top() < 0) {
            viewRect.moveTop(0);
        } else if (viewRect.bottom() > height()) {
            viewRect.moveBottom(height());
        }
    }

    if (viewRect.width() > width()) {
        if (viewRect.left() > 0) {
            viewRect.moveLeft(0);
        } else if (viewRect.right() < width()) {
            viewRect.moveRight(width());
        }
    } else {
        if (viewRect.left() < 0) {
            viewRect.moveLeft(0);
        } else if (viewRect.right() > width()) {
            viewRect.moveRight(width());
        }
    }

    return viewRect;
}

void ImageWidget::paintEvent(QPaintEvent *ev)
{
    Q_UNUSED(ev);

    QPainter painter(this);
    painter.fillRect(rect(), palette().window());
    if (m_image.isNull()) {
        return;
    }

    qreal scaleX = m_viewRect.width() / m_image.width();
    qreal scaleY = m_viewRect.height() / m_image.height();

    painter.save();
    if (qFuzzyIsNull(m_rotation)) {
        const float sx = qMax(-m_viewRect.x() / scaleX, 0.);
        const float sy = qMax(-m_viewRect.y() / scaleY, 0.);
        const float sw = qMin<float>(width() / scaleX, m_image.width());
        const float sh = qMin<float>(height() / scaleY, m_image.height());

        QRectF sourceRect(sx, sy, sw, sh);
        QRectF targetRect = rect() & m_viewRect.toAlignedRect();
        if (sourceRect.isEmpty() || targetRect.isEmpty()) {
            painter.restore();
            return;
        }

        if (Settings::smoothScale && sourceRect.width() * sourceRect.height() < 2000 * 2000) { // set an upper limit to avoid killing everything
            painter.drawImage(targetRect.topLeft(), m_image.copy(sourceRect.toRect()).scaled(targetRect.toRect().size(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation));
        } else {
            painter.drawImage(targetRect, m_image, sourceRect);
        }

    } else {
        painter.scale(scaleX, scaleY);
        QPoint center(m_viewRect.width() / 2, m_viewRect.height() / 2);
        painter.translate(center);
        painter.rotate(m_rotation);
        painter.translate(center * -1);
        QPoint upperLeft;
        if (m_viewRect.width() > m_image.width() * scaleX)
            upperLeft.setX(center.x() - scaleX*m_image.width() / 2);
        if (m_viewRect.height() > m_image.height() * scaleY)
            upperLeft.setY(center.y() - scaleY*m_image.height() / 2);
        painter.drawImage(upperLeft, m_image);
    }
    painter.restore();

    if (!m_cropRect.isEmpty()) {
        QRegion outsideCropRect = QRegion(m_viewRect.toAlignedRect()) - m_cropRect;
        for (const QRect &r : outsideCropRect) {
            painter.fillRect(r, QColor(0, 0, 0, 128));
        }
    }
}
