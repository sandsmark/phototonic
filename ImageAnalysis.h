#pragma once

#include "Histogram.h"

#include <QImage>
#include <QSet>

#include <cmath>

#ifdef __GNUC__
#define POPCNT(x) __builtin_popcountll(x)
#else
static inline uint64_t our_popcount64(uint64_t x)
{
  uint64_t m1 = 0x5555555555555555ll;
  uint64_t m2 = 0x3333333333333333ll;
  uint64_t m4 = 0x0F0F0F0F0F0F0F0Fll;
  uint64_t h01 = 0x0101010101010101ll;

  x -= (x >> 1) & m1;
  x = (x & m2) + ((x >> 2) & m2);
  x = (x + (x >> 4)) & m4;

  return (x * h01) >> 56;
}
#define POPCNT(x) our_popcount64(x)
#endif

class QProgressDialog;

struct ImageAnalysis
{
    static uint64_t calcImageHash(const QImage &inputImage, const QString &filePath);
    static Histogram calcHist(const QImage &img);


    void analyzeImage(const QString &filename, const QImage &inputImage);

    void sortBySimilarity(const bool withHash, QProgressDialog &progress);
    void selectRootIndices(const QStringList &selected);

    QList<Histogram> histograms;
    QList<QString> sortedFiles;
    QList<uint64_t> imageHashes;
    QSet<QString> loadedSortedFiles;

    uint64_t rootHash;
    Histogram rootHist;
    int rootHashIndex = 0;
    int rootHistIndex = 0;
};
