/*
 *  Copyright (C) 2018 Shawn Rutledge <s@ecloud.org>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QWidget>

struct Animation;

class ImageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ImageWidget(QWidget *parent = nullptr);
    bool empty();
    QImage image();
    void setImage(const QImage &i, bool refresh);
    qreal tempRotation() { return m_rotation; }
    void setTempRotation(qreal r);
    QPoint mapToImage(QPoint p);
    QPoint mapFromImage(QPoint p);
    QSize imageSize() const;
    void setCropPreview(const QRect &rect);
    const QRect &cropRect() const { return m_cropRect; }

    void setViewSize(const QSizeF viewSize, const QPointF &centerPoint);
    void setViewRect(const QRectF viewRect);
    QRectF viewRect() const { return m_viewRect; }
    void centerImage() {
        m_viewRect.moveCenter(rect().center());
    }

    bool loadAnimation(const QString &filename);
    bool animation() const;

protected:

    QSize sizeHint() const override;

    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

private slots:
    void onNextFrameReady(const QImage &frame);

private:
    QTransform imageMapTransform() const;

    QRectF constrainRect(QRectF viewRect) const;
    QRect m_cropRect;
    QRectF m_viewRect;
    QImage m_image;
    qreal m_rotation = 0;
    Animation *m_animation;
};

#endif // IMAGEWIDGET_H
