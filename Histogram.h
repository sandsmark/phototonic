#pragma once

#include <cmath>
#include <QPixmap>

#define HIST_DOWNSAMPLE 4

struct Histogram
{
    float red[256]{};
    float green[256]{};
    float blue[256]{};

    static inline float compareChannel(const float hist1[256], const float hist2[256])
    {
        float len1 = 0.f, len2 = 0.f, corr = 0.f;

        for (uint16_t i=0; i<256/HIST_DOWNSAMPLE; i++) {
            len1 += hist1[i];
            len2 += hist2[i];
            corr += std::sqrt(hist1[i] * hist2[i]);
        }

        const float part1 = 1.f / std::sqrt(len1 * len2);

        return std::sqrt(1.f - part1 * corr);
    }

    inline float compare(const Histogram &other) const
    {
        return compareChannel(red, other.red) +
            compareChannel(green, other.green) +
            compareChannel(blue, other.blue);
    }

    inline void downsample() {
        static_assert((256 % (256 / HIST_DOWNSAMPLE)) == 0);
        for (int target=0;target<256/HIST_DOWNSAMPLE; target++) {
            float avg[3]{};
            for (int i=0;i<HIST_DOWNSAMPLE; i++) {
                int idx = target * HIST_DOWNSAMPLE + i;
                avg[0] += red  [idx];
                avg[1] += green[idx];
                avg[2] += blue [idx];
            }
            red[target] =   avg[0] / HIST_DOWNSAMPLE;
            green[target] = avg[1] / HIST_DOWNSAMPLE;
            blue[target] =  avg[2] / HIST_DOWNSAMPLE;
        }
    }

    bool load(const QImage &image);
    QPixmap render(const QSize &size) const;
};
Q_DECLARE_METATYPE(Histogram);
