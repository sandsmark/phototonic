#pragma once

#include <exiv2/exiv2.hpp>
#include <exiv2/error.hpp>

#ifdef __clang__
#define IGNORE_EXIV2_DEPRECATED_START \
    _Pragma("GCC diagnostic push") \
    _Pragma("GCC diagnostic ignored \"-Wdeprecated-declarations\"")
#define IGNORE_EXIV2_DEPRECATED_END _Pragma("GCC diagnostic pop")
#else
#define IGNORE_EXIV2_DEPRECATED_START
#define IGNORE_EXIV2_DEPRECATED_END
#endif

