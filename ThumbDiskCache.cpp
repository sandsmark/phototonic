#include "ThumbDiskCache.h"

#include <QImageReader>
#include <QCryptographicHash>
#include <QFileInfo>
#include <QStandardPaths>
#include <QDir>
#include <QColorSpace>

QString ThumbDiskCache::thumbnailFileName(const QString &originalPath)
{
    QFileInfo info(originalPath);
    QString canonicalPath = info.canonicalFilePath();
    if (canonicalPath.isEmpty()) {
        qWarning() << originalPath << "does not exist!";
        canonicalPath = info.absoluteFilePath();
    }
    QUrl url = QUrl::fromLocalFile(canonicalPath);
    QCryptographicHash md5(QCryptographicHash::Md5);
    md5.addData(QFile::encodeName(url.adjusted(QUrl::RemovePassword).url()));
    return QString::fromLatin1(md5.result().toHex()) + QStringLiteral(".png");
}

static bool isAcceptableSize(const QSize &size, const QSize &targetSize, const bool shrinkable)
{
    if (size.width() < targetSize.width() && size.height() < targetSize.height()) {
        return false;
    }

    if (size.width() >= targetSize.width() && size.height() >= targetSize.height()) {
        return true;
    }

    // Need bigger than entire target size if not shrinkable
    if (!shrinkable) {
        return false;
    }

    return true;
}

QString ThumbDiskCache::locateThumbnail(const QString &originalPath, const QSize &targetSize, const bool shrinkable)
{
#if defined(Q_OS_MAC) || defined(Q_OS_WIN)
    return "";
#endif
    QStringList folders;

    int minSize = targetSize.width();
    if (shrinkable) {
        minSize = qMin(minSize, targetSize.height());
    }

    if (minSize <= 128) {
        folders.append(QStringLiteral("normal/")); // 128px max
    }
    if (minSize <= 256) {
        folders.append(QStringLiteral("large/")); // max 256px
    }
    if (minSize <= 512) {
        folders.append(QStringLiteral("x-large/")); // max 512px
    }
    folders.append(QStringLiteral("xx-large/")); // max 1024px

    const QString filename = thumbnailFileName(originalPath);
    const QString basePath = QStandardPaths::writableLocation(QStandardPaths::GenericCacheLocation) +
        QLatin1String("/thumbnails/");
    const QFileInfo originalInfo(originalPath);
    for (const QString &folder : folders) {
        QFileInfo info(basePath + folder + filename);
        if (!info.exists()) {
            continue;
        }
        if (originalInfo.metadataChangeTime() > info.lastModified()) {
            QFile::remove(info.absoluteFilePath());
            continue;
        }
        if (originalInfo.lastModified() > info.lastModified()) {
            QFile::remove(info.absoluteFilePath());
            continue;
        }
        QImageReader reader(info.absoluteFilePath());
        const QSize size = reader.size();
        // Need bigger than entire target size if not shrinkable
        if (!isAcceptableSize(size, targetSize, shrinkable)) {
            continue;
        }
        const QStringList text = reader.textKeys(); // check for the combined ThumbMTime to make sure we use qspng
        if (text.contains("ThumbMTime") && !text.contains("ThumbImageWidth")) {
            continue;
        }
        return info.absoluteFilePath();
    }
    return QString();
}

void ThumbDiskCache::storeThumbnail(const QString &originalPath, QImage thumbnail, const QSize &originalSize) {
#if defined(Q_OS_MAC) || defined(Q_OS_WIN)
    return;
#endif
    QFileInfo info(originalPath);
    const QString canonicalPath = info.canonicalFilePath();
    if (canonicalPath.isEmpty()) {
        qWarning() << "Asked to store thumbnail for non-existent path" << originalPath;
        return;
    }

    QString folder = QStringLiteral("normal/");
    const int maxSize = qMax(thumbnail.width(), thumbnail.height());
    if (maxSize < 64) {
        qDebug() << "Refusing to store tiny thumbnail" << thumbnail.size();
        return;
    }

    if (maxSize >= 512) {
        folder = QStringLiteral("xx-large/");
        thumbnail = thumbnail.scaled(1024, 1024, Qt::KeepAspectRatio, Qt::SmoothTransformation); // always smooth scale, we share these with other apps
    } else if (maxSize >= 256) { // x-large is max 512
        folder = QStringLiteral("x-large/");
    } else if (maxSize >= 128) { // large is max 256
        folder = QStringLiteral("large/");
    } else if (maxSize >= 64) {
        folder = QStringLiteral("normal/");
    } else {
        qWarning() << "Thumbnail too small" << thumbnail.size();
        return;
    }

    const QString filename = thumbnailFileName(originalPath);
    const QString basePath = QStandardPaths::writableLocation(QStandardPaths::GenericCacheLocation) +
        QLatin1String("/thumbnails/");

    if (!QFileInfo::exists(basePath + folder)) {
        QDir().mkpath(basePath + folder);
    }

    const QString fullPath = basePath + folder + filename;

    QDateTime lastModified = info.lastModified();
    if (info.metadataChangeTime() > info.lastModified()) {
        lastModified = info.metadataChangeTime();
    }
    thumbnail.setText(QStringLiteral("Thumb::MTime"), QString::number(lastModified.toSecsSinceEpoch()));

    QUrl url = QUrl::fromLocalFile(canonicalPath).adjusted(QUrl::RemovePassword);
    thumbnail.setText(QStringLiteral("Thumb::URI"), url.url());

    thumbnail.setText("Thumb::Size", QString::number(info.size()));

    thumbnail.setText(QStringLiteral("Thumb::Image::Width"), QString::number(originalSize.width()));
    thumbnail.setText(QStringLiteral("Thumb::Image::Height"), QString::number(originalSize.height()));
    thumbnail.setText("Software", "Phototonic");
    thumbnail.convertToColorSpace(QColorSpace::SRgb);

    // 64 bit images hits a slow path in qt and/or libpng, so avoid storing that
    if (thumbnail.depth() > 32) {
        thumbnail = thumbnail.convertToFormat(thumbnail.hasAlphaChannel() ? QImage::Format_ARGB32 : QImage::Format_RGB32);
    }

    // 81 maps to 8 compression via Qt's magic calculation, libpng goes from 0
    // to 9, where 9 is lossless.
    // 8 is still about 2-3 times as slow as 9, but it gives 1/3 the image size
    // for x-large thumbnails, so it's an okay tradeoff.
    // Anything less than 8 is useless for thumbnails since they are so small.
    if (!thumbnail.save(fullPath, "PNG", 81)) {
        qWarning() << "Failed to save to" << fullPath;
    }
}

static QHash<QString, QString> extractMetadata(QImageReader &thumbReader, QImage *thumb)
{
    const QHash<QString, QString> qimagereaderMapping = {
        {"ThumbImageWidth", "Thumb::Image::Width"},
        {"ThumbImageHeight", "Thumb::Image::Height"},
        {"ThumbMTime", "Thumb::MTime"},
        {"ThumbMTime", "Thumb::MTime"},
        {"ThumbSize", "Thumb::Size"},
        {"Software", "Software"},
    };
    QHash<QString, QString> ret;
    bool failed = false;
    for (const QString &key : qimagereaderMapping.keys()) {
        const QString value = thumbReader.text(key);
        if (value.isEmpty()) {
            qWarning() << "Missing" << key << thumbReader.textKeys() << ret;
            failed = true;
            break;
        }
        ret[qimagereaderMapping[key]] = value;
    }
    if (!failed) {
        return ret;
    }

    qDebug() << "Image reader failed";
    thumbReader.read(thumb);
    if (thumb->isNull()) {
        qWarning() << "Failed to read thumb";
        return {};
    }
    for (const QString &key : qimagereaderMapping.values()) {
        const QString value = thumb->text(key);
        if (value.isEmpty()) {
            qWarning() << "Missing" << key;
            return {};
        }
        ret[key] = value;
    }

    return ret;
}

static bool isValidThumbnail(QImageReader &thumbReader, const QString &originalFilename, const QSize &originalDimensions, QImage *thumb)
{
    const QHash<QString, QString> metadata = extractMetadata(thumbReader, thumb);
    if (metadata.isEmpty()) {
        qWarning() << "Failed to read metadata from thumbnail";
        return false;
    }

    bool ok = false;

    // This is cheapest to check, we already have all info and don't need to hit the disk
    if (!originalDimensions.isEmpty()) {
        int w = metadata["Thumb::Image::Width"].toInt(&ok);
        if (!ok) {
            qWarning() << "Failed to read original width";
            return false;
        }
        int h = metadata["Thumb::Image::Height"].toInt(&ok);
        if (!ok) {
            qWarning() << "Failed to read original height";
            return false;
        }
        const QSize fullSize(w, h);
        if (!originalDimensions.isEmpty() && !fullSize.isEmpty()) {
            if (w != originalDimensions.width() || h != originalDimensions.height()) {
                return false;
            }
        }

        if (fullSize.isEmpty()) {
            return false;
        }
    }

    QFileInfo fi(originalFilename);
    const long long timeSinceEpoch = metadata["Thumb::MTime"].toLongLong(&ok);
    if (!ok) {
        qWarning() << "Failed to read original mtime";
        return false;
    }
    const long long fileLastModified = qMax(fi.metadataChangeTime().toSecsSinceEpoch(), fi.lastModified().toSecsSinceEpoch());
    if (timeSinceEpoch != fileLastModified) {
        qWarning() << "Invalid last modified" << timeSinceEpoch << fileLastModified << fi.metadataChangeTime().toSecsSinceEpoch() << fi.lastModified().toSecsSinceEpoch();
        const bool fromKDE = metadata["Software"].startsWith("KDE Thumbnail Generator");

        // KDE is broken, so avoid us overwriting eachothers thumbnails
        if (!fromKDE || timeSinceEpoch != fi.lastModified().toSecsSinceEpoch()) {
            return false;
        }
    }

    const long long size = metadata["Thumb::Size"].toLongLong(&ok);
    if (!ok) {
        qWarning() << "Failed to read original file size";
        return false;
    }
    if (size != fi.size()) {
        qWarning() << "Invalid file size in thumbnail" << size << metadata["Thumb::Size"];
        return false;
    }

    if (thumb->isNull()) {
        thumbReader.read(thumb);
    }
    return true;
}

QImage ThumbDiskCache::loadThumbImage(const QString &imageFileName, const int targetSize, bool shrinkable) {
    QImageReader sourceReader;

    sourceReader.setFileName(imageFileName);
    sourceReader.setQuality(50); // 50 is the threshold where Qt does fast decoding, but still good scaling

    if (!sourceReader.canRead() && imageFileName.endsWith(".bmp")) {
        sourceReader.setFormat("dib");
    }

    if (!sourceReader.canRead()) {
        return {};
    }

    QSize sourceSize;

    if (!isSlow(sourceReader.format())) {
        sourceSize = sourceReader.size();
    }

    QSize outputSize = sourceSize;

    if (!outputSize.isEmpty()) {
        if (outputSize.width() < 64 && outputSize.width() < 64) {
            QImage image;
            sourceReader.read(&image);
            return image;
        }
        if (outputSize.width() > targetSize || outputSize.height() > targetSize) {
            outputSize.scale(QSize(targetSize, targetSize), shrinkable ?  Qt::KeepAspectRatio : Qt::KeepAspectRatioByExpanding);
        }
        sourceReader.setScaledSize(outputSize);
    } else {
        outputSize = QSize(targetSize, targetSize);
    }

    QString thumbnailPath = locateThumbnail(imageFileName, outputSize, shrinkable);
    if (!thumbnailPath.isEmpty()) {
        QImageReader thumbReader;
        thumbReader.setFileName(thumbnailPath);

        // avoid weird scaling
        if (!sourceSize.isEmpty()) {
            thumbReader.setScaledSize(outputSize);
        }

        QImage thumb;

        if (isValidThumbnail(thumbReader, imageFileName, sourceSize, &thumb)) {
            if (isAcceptableSize(thumb.size(), outputSize, shrinkable)) {
                return thumb;
            }

            if (thumb.size().width() >= outputSize.width() && thumb.size().height() >= outputSize.height()) {
                return thumb;
            }
        } else {
            if (!thumb.isNull()) {
                qWarning() << "Invalid thumbnail from software" << thumb.text("Software") << thumb.size();
            }
            qWarning() << "invalid thumbnail loaded, output size:" << outputSize << "thumb size" << thumb.size() << "original size" << sourceSize;
            qDebug() << "For file" << imageFileName;
            QFile::remove(thumbnailPath);
        }
    }

    QImage sourceImage;
    sourceReader.read(&sourceImage);

    // try again, without scaled size
    // We need to create a new reader, because that's how QImageReader works
    if (sourceImage.size().isEmpty() || !isAcceptableSize(sourceImage.size(), outputSize, shrinkable)) {
        QImageReader newReader(imageFileName);
        newReader.read(&sourceImage);

        // completely failed
        if (sourceImage.size().isEmpty()) {
            qWarning() << "Completely failed to load" << QFileInfo(imageFileName).fileName();
            return sourceImage;
        }
    }

    if (sourceImage.isNull()) {
        qWarning() << "null source image";
        return {};
    }
    if (sourceSize.isEmpty()) {
        // only set if it isn't empty, if it's not empty we set scaled size when loading and this is wrong
        sourceSize = sourceImage.size();
    }

    QImage thumb;
    if (sourceImage.width() > targetSize || sourceImage.height() > targetSize) {
        thumb = sourceImage.scaled(QSize(targetSize, targetSize), shrinkable ?  Qt::KeepAspectRatio : Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
    } else {
        thumb = sourceImage;
    }

    storeThumbnail(imageFileName, thumb, sourceSize);

    return sourceImage;
}

bool ThumbDiskCache::getOriginalMetadata(const QString &originalPath, QSize *dimensions, bool deleteFailed)
{
    QString thumbnailPath = locateThumbnail(originalPath, QSize(1, 1), true);
    if (thumbnailPath.isEmpty()) {
        return false;
    }
    QImageReader thumbReader(thumbnailPath);
    QImage dummy;

    const QHash<QString, QString> metadata = extractMetadata(thumbReader, &dummy);
    if (metadata.isEmpty()) {
        qWarning() << "Failed to read metadata from thumbnail" << originalPath << thumbnailPath;
        if (deleteFailed) {
            QFile::remove(thumbnailPath);
        }
        return false;
    }
    bool ok = false;
    int w = metadata["Thumb::Image::Width"].toInt(&ok);
    if (!ok) {
        qWarning() << "Failed to read original width";
        if (deleteFailed) {
            QFile::remove(thumbnailPath);
        }
        return false;
    }
    int h = metadata["Thumb::Image::Height"].toInt(&ok);
    if (!ok) {
        qWarning() << "Failed to read original height";
        if (deleteFailed) {
            QFile::remove(thumbnailPath);
        }
        return false;
    }
    *dimensions = QSize(w, h);

    return w > 0 && h > 0;
}
