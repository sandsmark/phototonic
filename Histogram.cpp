#include "Histogram.h"
#include <QPainter>
#include <QColorSpace>

bool Histogram::load(const QImage &input)
{
    if (input.isNull()) {
        return false;
    }
    QImage image = input;

    if (image.colorSpace().isValid() && image.colorSpace() != QColorSpace::SRgb) {
        image.convertToColorSpace(QColorSpace::SRgb);
    }
    if (image.format() != QImage::Format_RGB32 && image.format() != QImage::Format_ARGB32) {
        image.convertTo(QImage::Format_RGB32);
    }

    bzero(red, sizeof red);
    bzero(green, sizeof green);
    bzero(blue, sizeof blue);
    const int width = image.width();
    const int height = image.height();
    for (int y=0; y<height; y++) {
        const uchar *line = image.constScanLine(y);
        for (int x=0; x<width*4; x += 4) {
            red  [line[x + 0]] += 1.f;
            green[line[x + 1]] += 1.f;
            blue [line[x + 2]] += 1.f;
        }
    }
    return true;
}

QPixmap Histogram::render(const QSize &size) const {
    QPointF redPoly[258];
    QPointF greenPoly[258];
    QPointF bluePoly[258];

    float maxY = 0;
    maxY = 0;
    for (int i=0; i<256; i++) {
        maxY = qMax(red[i], qMax(green[i], qMax(blue[i], maxY)));
    }

    const qreal height = size.height();
    const qreal width = size.width();

    // Start and end points
    redPoly[0] = greenPoly[0] = bluePoly[0] = QPointF(0, height);
    redPoly[257] = greenPoly[257] = bluePoly[257] = QPointF(width, height);

    const qreal multY = qFuzzyIsNull(maxY) ? 0. : 1. / maxY;

    QPointF *rp = &redPoly[1];
    QPointF *gp = &greenPoly[1];
    QPointF *bp = &bluePoly[1];

    for (int i=0; i<256; i++) {
        const qreal x = i * width / 256.;
        rp[i] = QPointF(x, height - height * std::sqrt(red[i]   * multY));
        gp[i] = QPointF(x, height - height * std::sqrt(green[i] * multY));
        bp[i] = QPointF(x, height - height * std::sqrt(blue[i]  * multY));
    }

    QPixmap rendered(size);
    rendered.fill(Qt::black);

    QPainter p(&rendered);
    p.setRenderHint(QPainter::Antialiasing);
    p.setPen(Qt::transparent);

     // additive blending, red over blue becomes purple etc.
    p.setCompositionMode(QPainter::CompositionMode_Plus);

    p.setBrush(Qt::blue);
    p.drawConvexPolygon(bluePoly, 258);

    p.setBrush(Qt::red);
    p.drawConvexPolygon(redPoly, 258);

    p.setBrush(Qt::green);
    p.drawConvexPolygon(greenPoly, 258);


    p.end();

    return rendered;
}
