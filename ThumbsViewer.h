/*
 *  Copyright (C) 2013 Ofer Kashayov <oferkv@live.com>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef THUMBS_VIEWER_H
#define THUMBS_VIEWER_H

#include <QListView>
#include <QTimer>
#include "Settings.h"
#include "DirLister.h"
#include "ImageAnalysis.h"

#define BATCH_SIZE 10

class Phototonic;
class MetadataCache;
class ImageViewer;
class ImageTags;
class InfoView;
class ImagePreview;

class QStandardItem;
class QStandardItemModel;

struct DuplicateImage
{
    QString filePath;
    int duplicates = 0;
    int id = 0;
    qreal sortOrder = 0.;
};

class ThumbsViewer : public QListView {
Q_OBJECT

public:
    enum UserRoles {
        FileNameRole = Qt::UserRole + 1,
        FileNameSortRole = Qt::UserRole + 1,
        SortRole,
        LoadedRole,
        BrightnessRole,
        TypeRole,
        SizeRole,
        TimeRole,
        SimilarityRole,
        DuplicateOrder,
        ResolutionRole,
        RoleCount
    };
    enum ThumbnailLayouts {
        Classic,
        Squares,
        Compact
    };
    enum SimilarityMode {
        ColorSimilarity,
        ContentSimilarity
    };

    ThumbsViewer(QWidget *parent, const std::shared_ptr<MetadataCache> &metadataCache);

    void updateItemLayout();

    void applyFilter();

    void reLoad();

    void loadDuplicates();

    void loadFileList();

    void loadSubDirectories();

    void setThumbColors();

    bool setCurrentIndexByName(QString &fileName);

    bool setCurrentIndexByRow(int row);

    void setCurrentRow(int row);

    void setImageViewerWindowTitle();

    void setNeedToScroll(bool needToScroll);

    void selectCurrentIndex();

    QStandardItem *addThumb(const QString &imageFullPath, qreal sortValue = -1);

    void abort(bool permanent = false);

    void selectThumbByRow(int row);

    void selectByBrightness(qreal min, qreal max);

    int getNextRow();

    int getPrevRow();

    int getLastRow();

    int getRandomRow();

    int getCurrentRow();

    QStringList getSelectedThumbsList();

    QString getSingleSelectionFilename();

    void setImageViewer(ImageViewer *imageViewer);

    void sortBySimilarity(const SimilarityMode mode);

    void refreshThumbnails(bool clearContents);

    InfoView *infoView;
    ImagePreview *imagePreview;
    ImageTags *imageTags;
    DirLister thumbsDir;
    QStringList fileFilters;
    QStandardItemModel *thumbsViewerModel;
    QDir::SortFlags thumbsSortFlags;
    int thumbSize;
    bool isBusy;

protected:
    void startDrag(Qt::DropActions) override;
    void mousePressEvent(QMouseEvent *event) override;

    // There should be a signal for this
    void resizeEvent(QResizeEvent *event) override {
        QListView::resizeEvent(event);
        updateVisibleThumbnails();
    }

private:
    bool loadExivTags(const QString &imageFullPath);

    const QPixmap &getErrorPixmap();

    void initThumbs();

    bool loadThumb(int row);

    void resetModel();

    void findDupes(bool resetCounters);

    int getFirstVisibleThumb();

    int getLastVisibleThumb();

    void updateThumbsCount(const bool scanning = false);

    void updateFoundDupesState(int duplicates, int filesScanned, int originalImages);

    void updateImageInfoViewer(int row);

    QSize itemSizeHint() const;

    void ensureHistogramsAndHashes(QProgressDialog *progress);

    void loadVisibleThumbs(int scrollBarValue = 0);

    ImageAnalysis analysis;

    QFileInfo thumbFileInfo;
    QFileInfoList thumbFileInfoList;

    QHash<QString, int> nameSortValues;

    QPixmap errorPixmap;
    QPixmap emptyImg;
    QModelIndex m_currentIndex;
    Phototonic *phototonic;
    std::shared_ptr<MetadataCache> metadataCache;
    ImageViewer *imageViewer;
    QHash<uint64_t, DuplicateImage> dupImageHashes;
    bool isAbortThumbsLoading = false;
    bool isClosing = false;
    bool isNeedToScroll = false;
    int currentRow = 0;
    bool scrolledForward = false;
    int thumbsRangeFirst;
    int thumbsRangeLast;

    bool reloadVisible = false;

    QTimer m_selectionChangedTimer;
    QTimer m_loadThumbTimer;
    QTimer m_loadVisibleTimer;

public slots:
    void onOrderingChanged() {
        reloadVisible = true;
        updateVisibleThumbnails();
    }

    void updateVisibleThumbnails() {
        if (!m_loadVisibleTimer.isActive()) {
            m_loadVisibleTimer.start();
        }
    }

    void onSelectionChanged();

    void invertSelection();

    void reloadFile(const QString &filename);

private slots:

    void loadThumbsRange();

    void loadAllThumbs();
};

#endif // THUMBS_VIEWER_H

