#pragma once

#include <QDir>
#include <QSet>
#include <QMimeDatabase>

struct FormatProber
{
    enum ImageType {
        NotImage = 0,
        WebM,
        Avif,
        Avis,
        Heic,
        WebP,
        Mp4,
        Matroska,
        Ico,
        Png,
        Jpeg,
        Gif,
        Bmp,
        Dib,
        OtherImageFormat
    };

    static ImageType imageType(const QString &filePath) {
        return instance().probe(filePath);
    }

    static bool isLossless(const QString &filePath) {
        switch(instance().probe(filePath)) {
        case Png:
        case Bmp:
        case Gif:
            return true;

        case WebP:
            //return instance().isWebPLossless(filePath);
            // todo: support for other modern formats like heic and avif
        default:
            return false;
        }
    }


private:
    static FormatProber &instance() {
        static FormatProber inst;
        return inst;
    }

    FormatProber();
    static constexpr int probeSize = 13;
    ImageType probe(const QString &filePath) const;

#if 0
    bool isWebPLossless(const QString &filePath) const;
#endif

    bool webmSupported = false;
    bool avifSupported = false;
    bool heicSupported = false;
    bool webpSupported = false;
    bool mp4Supported = false;
    bool matroskaSupported = false;
    bool icoSupported = false;
    QSet<QString> supportedMimetypes;
};

struct DirLister
{
    QFileInfoList entryInfoList();
    void setPath(const QString &path) {
        dir.setPath(path);
    }
    void setNameFilters(const QStringList &filters) {
        nameFilters = filters;

        suffixes.clear();
        for (const QString &suffixFilter : filters) {
            if (suffixFilter.startsWith("*.") && suffixFilter.count('.') == 1) {
                suffixes.insert(suffixFilter.mid(2).toLower());
                continue;
            }
        }
    }
    QDir::Filters filter() const {
        return dir.filter();
    }
    void setFilter(const QDir::Filters filter) {
        dir.setFilter(filter);
    }
    void setSorting(const QDir::SortFlags sort) {
        dir.setSorting(sort);
    }

    QSet<QString> suffixes;
    QStringList nameFilters;
    QString filterString;
    QDir dir;
};
