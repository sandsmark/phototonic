#pragma once

// Pure C Implementation.
void ahsv_from_argb_c(float* dst, const float* src, int length);
void argb_from_ahsv_c(float* dst, const float* src, int length);

#if __SSE2__ && __OPTIMIZE__
// SSE2 Enhanced Implementation.
void ahsv_from_argb(float* dst, const float* src, int length);
void argb_from_ahsv(float* dst, const float* src, int length);
#else
#define ahsv_from_argb ahsv_from_argb_c
#define argb_from_ahsv argb_from_ahsv_c
#endif
