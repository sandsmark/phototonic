// Mostly taken from Blitz, hacked up and put together again
//
//   Copyright (c) 2003-2007 Clarence Dang <dang@kde.org>
//   Copyright (c) 2006 Mike Gashler <gashlerm@yahoo.com>
//   All rights reserved.

//   Redistribution and use in source and binary forms, with or without
//   modification, are permitted provided that the following conditions
//   are met:

//   1. Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//   2. Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.

//   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//   IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <QImage>

#define CONVOLVE_ACC(weight, pixel)  \
    r += /* ((weight))* */(qRed((pixel)));   \
    g += /* ((weight))* */(qGreen((pixel))); \
    b += /* ((weight))* */(qBlue((pixel)));

bool mean(QImage &img)
{
    // basically just a hack, inlined a convolution filter
    static constexpr int matrix_size = 7;
    static constexpr int matrix[matrix_size * matrix_size] = {
        1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1,
    };
    const int divisor = std::accumulate(std::begin(matrix), std::end(matrix), 0);

    static constexpr int edge = matrix_size / 2;

    if (!(matrix_size % 2)) {
        qWarning("Blitz::convolve(): kernel width must be an odd number!");
        return false;
    }

    const int w = img.width();
    const int h = img.height();

    if (w < 3 || h < 3) {
        qWarning("Blitz::convolve(): Image is too small!");
        return false;
    }

    if (img.hasAlphaChannel()) {
        if (img.format() != QImage::Format_ARGB32) {
            img.convertTo(QImage::Format_ARGB32);
        }
    } else if (img.format() != QImage::Format_RGB32) {
        img.convertTo(QImage::Format_RGB32);
    }

    QImage buffer(w, h, img.format());
    QRgb *scanblock[matrix_size];

    for (int y = 0; y < h; ++y) {
        const QRgb *src = (QRgb *)img.constScanLine(y);
        QRgb *dest = (QRgb *)buffer.scanLine(y);
        // Read in scanlines to pixel neighborhood. If the scanline is outside
        // the image use the top or bottom edge.
        int x = y - edge;

        for (int i = 0; x <= y + edge; ++i, ++x) {
            scanblock[i] = (QRgb *)
                           img.constScanLine((x < 0) ? 0 : (x > h - 1) ? h - 1 : x);
        }

        // Now we are about to start processing scanlines. First handle the
        // part where the pixel neighborhood extends off the left edge.
        for (x = 0; x - edge < 0 ; ++x) {
            int r = 0, g = 0, b = 0;

            int mPos = 0;
            for (int matrix_y = 0; matrix_y < matrix_size; ++matrix_y) {
                const QRgb *s = scanblock[matrix_y];
                int matrix_x = -edge;

                while (x + matrix_x < 0) {
                    CONVOLVE_ACC(matrix[mPos], *s);
                    ++matrix_x;
                    ++mPos;
                }

                while (matrix_x <= edge) {
                    CONVOLVE_ACC(matrix[mPos], *s);
                    ++matrix_x;
                    mPos++;
                    ++s;
                }
            }

            r /= divisor;
            g /= divisor;
            b /= divisor;
            *dest++ = qRgba(r, g, b, qAlpha(*src++));
        }

        // Okay, now process the middle part where the entire neighborhood
        // is on the image.
        for (; x + edge < w; ++x) {
            int r = 0, g = 0, b = 0;
            int mPos = 0;

            for (int matrix_y = 0; matrix_y < matrix_size; ++matrix_y) {
                const QRgb *s = scanblock[matrix_y] + (x - edge);

                for (int matrix_x = -edge; matrix_x <= edge; ++matrix_x, ++mPos, ++s) {
                    CONVOLVE_ACC(matrix[mPos], *s);
                }
            }

            r /= divisor;
            g /= divisor;
            b /= divisor;
            *dest++ = qRgba(r, g, b, qAlpha(*src++));
        }

        // Finally process the right part where the neighborhood extends off
        // the right edge of the image
        for (; x < w; ++x) {
            int r = 0, g = 0, b = 0;
            int mPos = 0;

            for (int matrix_y = 0; matrix_y < matrix_size; ++matrix_y) {
                const QRgb *s = scanblock[matrix_y];
                s += x - edge;
                int matrix_x = -edge;

                while (x + matrix_x < w) {
                    CONVOLVE_ACC(matrix[mPos], *s);
                    ++matrix_x;
                    ++mPos;
                    ++s;
                }

                --s;

                while (matrix_x <= edge) {
                    CONVOLVE_ACC(matrix[mPos], *s);
                    ++matrix_x;
                    ++mPos;
                }
            }

            r /= divisor;
            g /= divisor;
            b /= divisor;
            *dest++ = qRgba(r, g, b, qAlpha(*src++));
        }
    }

    img = buffer;
    return true;
}

