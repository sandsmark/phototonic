#pragma once

#if  __SSE__ || __SSE2__ || __SSE3__

#include <math.h>
#include <stdlib.h>

// SSE.
#if __SSE__
#include <xmmintrin.h>
#endif

// SSE2.
#if __SSE2__
#include <emmintrin.h>
#endif

// SSSE3.
#if __SSE3__
#include <tmmintrin.h>
#endif

#if defined(_MSC_VER)
# define INLINE __forceinline
# include <windows.h>
#else//defined(_MSC_VER)
# define INLINE inline
# include <sys/time.h>
#endif//defined(_MSC_VER)

// ============================================================================
// [SSE / SSE2]
// ============================================================================

#if defined(_MSC_VER)
#define XMM_ALIGNED_VAR(_Type_, _Name_) \
  __declspec(align(16)) _Type_ _Name_
#else // _MSC_VER
#define XMM_ALIGNED_VAR(_Type_, _Name_) \
  _Type_ __attribute__((aligned(16))) _Name_
#endif // _MSC_VER

#define XMM_CONSTANT_PS(name, val0, val1, val2, val3) \
  XMM_ALIGNED_VAR(static const float, _xmm_const_##name[4]) = { val3, val2, val1, val0 }

#define XMM_CONSTANT_PI(name, val0, val1, val2, val3) \
  XMM_ALIGNED_VAR(static const int, _xmm_const_##name[4]) = { int(val3), int(val2), int(val1), int(val0) }

#define XMM_GET_SS(name) (*(const  float  *)_xmm_const_##name)
#define XMM_GET_PS(name) (*(const __m128  *)_xmm_const_##name)
#define XMM_GET_SI(name) (*(const int     *)_xmm_const_##name)
#define XMM_GET_PI(name) (*(const __m128i *)_xmm_const_##name)

// Shuffle Float4 by using SSE2 instruction.
#define XMM_SHUFFLE_PS(src, imm) \
  _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(src), imm))

static __m128 INLINE _mm_load_ps_my(const void* mem) { return _mm_load_ps((const float*)mem); }
static void INLINE _mm_store_ps_my(void* mem, __m128 reg) { _mm_store_ps((float*)mem, reg); }

// ============================================================================
// [Timer]
// ============================================================================

struct Timer {
  INLINE Timer() : cnt(0) {}
  INLINE unsigned int get() const { return cnt; }
  INLINE void start() { cnt = sGetTime(); }
  INLINE void stop() { cnt = sGetTime() - cnt; }

  static INLINE unsigned int sGetTime() {
#if defined(_MSC_VER)
    return GetTickCount();
#else
    timeval ts;
    gettimeofday(&ts,0);
    return (unsigned int)(ts.tv_sec * 1000 + (ts.tv_usec / 1000));
#endif
  }

  unsigned int cnt;
};

#endif//__SSE__ || __SSE2__ || __SSE3__
