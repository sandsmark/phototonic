#include "SmartCrop.h"
#include <QDebug>
#include <QtMath>
#include <QElapsedTimer>

static const bool s_benchmark = qEnvironmentVariableIsSet("PHOTOTONIC_BENCH");

namespace SmartCrop {

// Branchless, pretty massive speed improvement
#define MIN_INT_(x, y) (y ^ ((x ^ y) & -(x < y)))
#define MAX_INT_(x, y) (x ^ ((x ^ y) & -(x < y)))
#define MIN_INT(x, y) MIN_INT_(int(x), int(y))
#define MAX_INT(x, y) MAX_INT_(int(x), int(y))

#define MAX(x,y) (x > y ? x : y)
#define MIN(x,y) (x < y ? x : y)

static inline double fastPow(const double a, const double b) {
    union {
        double d;
        int x[2];
    } u = { a };
    u.x[1] = int(b * (u.x[1] - 1072632447) + 1072632447);
    u.x[0] = 0;
    return u.d;
}

static inline qreal cie(qreal r, qreal g, qreal b) {
    double var_R = r * (1./ 255.);
    if (var_R > 0.04045) {
        var_R = fastPow(((var_R + 0.055) * (1. / 1.055)), 2.4);
    } else {
        var_R = var_R  * (1./ 12.92);
    }

    double var_G = g * (1./ 255.);
    if (var_G > 0.04045) {
        var_G = fastPow(((var_G + 0.055) * (1./ 1.055)), 2.4);
    } else {
        var_G = var_G * (1./ 12.92);
    }

    double var_B = b * (1./ 255.);
    if (var_B > 0.04045) {
        var_B = fastPow(((var_B + 0.055) * (1. / 1.055)), 2.4);
    } else {
        var_B = var_B  * (1./ 12.92);
    }

    //Observer. = 2°, Illuminant = D65
    double Y = 100 * var_R * 0.2126 + 100 * var_G * 0.7152 + 100 * var_B * 0.0722;

    double var_Y = Y * (1./ 100.000);

    if (var_Y > 0.008856) {
        var_Y = fastPow(var_Y, (1.0 / 3));
    } else {
        var_Y = (7.787 * var_Y) + (16.0 / 116);
    }

    return MAX((295.0 * var_Y) - 40.8, 0);
}

template<int R, int G, int B, int A>
static void edgeDetect(const QImage &i, QImage &o) {
    const int bpl = i.bytesPerLine();
    const uchar *id = i.scanLine(0);
    const int h = i.height();
    const int w = i.width();

    std::vector<uchar> row(w, 0);
    uchar *rd = row.data();

    for (int y = 1; y < h-1; y++) {
        uchar *od = o.scanLine(y);
        for (int x = 1; x < w-1; x++) {
            const int p = y * bpl + x * 4;
            const int lightness = id[p + A] * 4 -
                id[p - 4 + A] -
                id[p + 4 + A] -
                id[p - bpl + A] -
                id[p + bpl + A];

            rd[x] = MAX_INT(lightness, 0);
        }
        for (int x = 0; x < w; x++) {
            od[x * 4 + G] = rd[x];
        }
    }

    // Stretch out onto the edges
    uchar *od = o.scanLine(0);
    for (int x=0; x<w; x++) {
        od[x * 4 + G] = od[x * 4 + bpl + G];
    }
    od = o.scanLine(h-1);
    for (int x=0; x<w; x++) {
        od[x * 4 + G] = od[x * 4 - bpl + G];
    }
    od = o.scanLine(0);
    const int eol = (w - 1) * 4;
    for (int y=0; y<h; y++) {
        od[y * bpl + G] =       od[y * bpl + 4 + G];
        od[y * bpl + eol + G] = od[y * bpl + eol - 4 + G];
    }
}

static inline unsigned skinColor(const CropOptions &options, uchar r, uchar g, uchar b) {
    union {
        float y;
        int i;
    } inv;
    static_assert(sizeof(inv.y) == sizeof(inv.i));

	inv.y = r * r + g * g + b * b;
	const float x2 = inv.y * 0.5F;
	inv.i = 0x5f3759df - (inv.i >> 1);
	inv.y = inv.y * (1.5f - (x2 * inv.y * inv.y));

    const float rd = r * inv.y - (options.skinColor[0]);
    const float gd = g * inv.y - (options.skinColor[1]);
    const float bd = b * inv.y - (options.skinColor[2]);
    return 255 - (sqrt(rd * rd + gd * gd + bd * bd)) * 255;
}

template<int R, int G, int B, int A>
static void skinDetect(const CropOptions &options, const QImage &i, QImage &o) {
    const int w = i.width();
    const int h = i.height();

    std::vector<uchar> row(w, 0);
    for (int y = 0; y < h; y++) {
        row.assign(w, 0);
        uchar *rd = row.data();
        const uchar *id = i.scanLine(y);
        for (int p=0, x=0; p<w*4; p+=4, x++ ){
            const uchar lightness = id[p+A];
            if (lightness < options.skinBrightnessMin || lightness > options.skinBrightnessMax) {
                continue;
            }
            const uchar r = id[p+R];
            const uchar g = id[p+G];
            const uchar b = id[p+B];
            if (!r && !g && !b) {
                continue;
            }

            const unsigned skin = skinColor(options, r, g, b);
            if (skin < options.skinThreshold) {
                continue;
            }
            rd[x] = 255u * (skin - options.skinThreshold) /
                (255u - options.skinThreshold);
        }

        uchar *od = o.scanLine(y);
        for (int p = 0, x=0; p < w * 4; p+=4, x++) {
            od[p + R] = rd[x];
        }
    }
}

static inline int saturation(int r, int g, int b) {
    const int maximum = MAX_INT(r, MAX_INT(g, b));
    const int minimum = MIN_INT(r, MIN_INT(g, b));

    if (maximum == minimum) {
        return 0;
    }

    const unsigned l = (maximum + minimum) / 2;
    const unsigned d = 255 * (maximum - minimum);

    return (l > 128 ? d / (512 - maximum - minimum) : d / (maximum + minimum));
}

template<int R, int G, int B, int A>
static void saturationDetect(const CropOptions &options, const QImage &i, QImage &o) {
    int w = i.width();
    int h = i.height();

    std::vector<uchar> row(w, 0);
    static constexpr qreal saturationMult = (255. / (255. - CropOptions::saturationThreshold));
    for (int y = 0; y < h; y++) {
        uchar *od = o.scanLine(y);
        row.assign(w, 0);
        uchar *rd = row.data();
        const uchar *id = i.scanLine(y);
        for (int x = 0; x < w; x++, id += 4) {
            const uchar lightness = id[A];
            if (lightness < options.saturationBrightnessMin ||
                    lightness > options.saturationBrightnessMax) {
                continue;
            }

            const unsigned sat = saturation(id[R], id[G], id[B]);
            if (sat < options.saturationThreshold) {
                continue;
            }
            rd[x] = (sat - options.saturationThreshold) * saturationMult;
        }
        for (int x = 0; x < w; x++) {
            od[x * 4 + B] = rd[x];
        }
    }
}

template<int R, int G, int B, int A>
static QImage downSample(QImage &input, qreal factor) {
    int width = qFloor(input.width() / factor);
    int height = qFloor(input.height() / factor);
    static QImage output;
    output = QImage(width, height, input.format());
    qreal ifactor2 = 1. / (factor * factor);
    for (int y = 0; y < height; y++) {
        uchar *data = output.scanLine(y);
        for (int x = 0; x < width; x++) {
            int i = x * 4;

            int r = 0;
            int g = 0;
            int b = 0;

            int mr = 0;
            int mg = 0;

            for (int v = 0; v < factor; v++) {
                uchar *idata = input.scanLine(y * factor + v);
                for (int u = 0; u < factor; u++) {
                    int j = (x * factor + u) * 4;
                    r += idata[j + R];
                    g += idata[j + G];
                    b += idata[j + B];
                    mr = MAX_INT(mr, int(idata[j + R]));
                    mg = MAX_INT(mg, int(idata[j + G]));
                }
            }
            // this is some funky magic to preserve detail a bit more for
            // skin (r) and detail (g). Saturation (b) does not get this boost.
            data[i + A] = 255;
            data[i + R] = r * ifactor2 * 0.5 + mr * 0.5;
            data[i + G] = g * ifactor2 * 0.7 + mg * 0.3;
            data[i + B] = b * ifactor2;
        }
    }
    return output;
}

// Gets value in the range of [0, 1] where 0 is the center of the pictures
// returns weight of rule of thirds [0, 1]
static inline qreal thirds(qreal x) {
    x = ((int(x - 1. / 3. + 1.0) % 2) * 0.5 - 0.5) * 16.;
    return MAX(1.0 - x * x, 0.0);
}

static inline qreal importance(const CropOptions &options, const QRectF &crop, qreal x, qreal y) {
    if (
            crop.x() > x ||
            x >= crop.x() + crop.width() ||
            crop.y() > y ||
            y >= crop.y() + crop.height()
            ) {
        return options.outsideImportance;
    }
    x = (x - crop.x()) / qreal(crop.width());
    y = (y - crop.y()) / qreal(crop.height());
    qreal px = abs(0.5 - x) * 2.;
    qreal py = abs(0.5 - y) * 2.;
    // Distance from edge
    qreal dx = MAX(px - 1.0 + options.edgeRadius, 0.);
    qreal dy = MAX(py - 1.0 + options.edgeRadius, 0.);
    qreal d = (dx * dx + dy * dy) * options.edgeWeight;
    qreal s = 1.41 - sqrt(px * px + py * py);
    if (options.ruleOfThirds) {
        s += MAX(0., s + d + 0.5) * 1.2 * (thirds(px) + thirds(py));
    }
    return s + d;
}

static QList<QRectF> generateCrops(const CropOptions &options, qreal width, qreal height) {
    QList<QRectF> results;
    int minDimension = qMin(width, height);
    qreal cropWidth = options.cropWidth > 0 ? options.cropWidth : minDimension;
    qreal cropHeight = options.cropHeight > 0 ? options.cropHeight : minDimension;
    for (
         qreal scale = options.maxScale;
         scale >= options.minScale;
         scale -= options.scaleStep
         ) {
        for (qreal y = 0; y + cropHeight * scale <= height; y += options.step) {
            for (qreal x = 0; x + cropWidth * scale <= width; x += options.step) {
                results.append({x, y, cropWidth * scale, cropHeight * scale});
            }
        }
    }
    return results;
}

template<int R, int G, int B, int A>
static float score(const CropOptions &options, const QImage &output, const QRectF &crop) {
    qreal detail = 0;
    qreal saturation = 0;
    qreal skin = 0;
    qreal boost = 0;

    const qreal downSample = options.scoreDownSample;
    const qreal invDownSample = 1. / downSample;
    const qreal outputHeightDownSample = output.height() * downSample;
    const qreal outputWidthDownSample = output.width() * downSample;

    for (qreal y = 0; y < outputHeightDownSample; y += downSample) {
        const uchar *od = output.scanLine(int(y * invDownSample));
        for (qreal x = 0; x < outputWidthDownSample; x += downSample) {
            const int p = int(x * invDownSample) * 4;
            const qreal i = importance(options, crop, x, y);
            const qreal dtl = od[p + G] / 255.;

            skin += (od[p + R]  /255.) * (dtl + options.skinBias) * i;
            detail += dtl * i;
            saturation += (od[p + B]  / 255.) * (dtl + options.saturationBias) * i;
            boost += (od[p + A]  / 255.) * i;
        }
    }

    return (
            detail * options.detailWeight +
            skin * options.skinWeight +
            saturation * options.saturationWeight
            );
}

//template<int R, int G, int B, int A>
QRect smartCropRect(const QImage &input, CropOptions options)
{
    static constexpr int A = 3;
    static constexpr int R = 2;
    static constexpr int G = 1;
    static constexpr int B = 0;

    if (input.isNull()) {
        qWarning() << "Invalid image";
        return QRect();
    }

    if (options.aspect) {
        options.width = options.aspect;
        options.height = 1;
    }

    QImage image;
    switch(input.format()) {
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            image = input;
            break;
        default:
            qWarning() << "Need to convert format from" << input.format();
            image = input.convertToFormat(QImage::Format_ARGB32);
            break;
    }

    qreal scale = 1;
    qreal prescale = 1;
    if (options.width && options.height) {
        scale = qMin(
                    image.width() / options.width,
                    image.height() / options.height
                    );
        options.cropWidth = qFloor(options.width * scale);
        options.cropHeight = qFloor(options.height * scale);
        // Img = 100x100, width = 95x95, scale = 100/95, 1/scale > min
        // don't set minscale smaller than 1/scale
        // -> don't pick crops that need upscaling
        options.minScale = qMin(
                    options.maxScale,
                    qMax(1 / scale, options.minScale)
                    );

        // prescale if possible
        if (options.prescale != false) {
            prescale = qMin(qMax(256. / image.width(), 256. / image.height()), 1.);
            if (prescale < 1) {
                image = image.scaled(image.width() * prescale, image.height () * prescale);
                options.cropWidth = qFloor(options.cropWidth * prescale);
                options.cropHeight = qFloor(options.cropHeight * prescale);
            } else {
                prescale = 1;
            }
        }
    }

    QElapsedTimer timer;
    timer.start();
    {
        // Since a ton of things use the CIE values, we precalc them and store
        // them in the alpha channel of the image
        const int iw = image.width();
        const int ih = image.height();
        std::vector<uchar> row(image.width(), 0);

        for (int y = 0; y < ih; y++) {
            uchar *rowData = row.data();
            uchar *id = image.scanLine(y);
            for (int x = 0; x < iw; x++) {
                const int p = x * 4;
                rowData[x] = cie(id[p + R], id[p + G], id[p + B]);
            }
            for (int x = 0; x < iw; x++) {
                id[x*4 + A] = rowData[x];
            }
        }
    }
    if (s_benchmark) qDebug() << "cie in" << timer.restart() << "ms";

    QImage filtered = QImage(image.size(), image.format());

    if (s_benchmark) qDebug() << "filtered created in" << timer.restart() << "ms";
    edgeDetect<R,G,B,A>(image, filtered);
    if (s_benchmark) qDebug() << "edgedetect in" << timer.restart() << "ms";
    skinDetect<R,G,B,A>(options, image, filtered);
    if (s_benchmark) qDebug() << "skindetect in" << timer.restart() << "ms";
    saturationDetect<R,G,B,A>(options, image, filtered);
    if (s_benchmark) qDebug() << "saturation in" << timer.restart() << "ms";
    QImage toScore = downSample<R,G,B,A>(filtered, options.scoreDownSample);
    if (s_benchmark) qDebug() << "downsample in" << timer.restart() << "ms";

    QList<QRectF> crops = generateCrops(options, image.width(), image.height());
    if (s_benchmark) qDebug() << "crops in" << timer.restart() << "ms";


    QRectF topCrop;
    qreal topScore = -1;
    for (const QRectF &crop : crops) {
        float scr = score<R,G,B,A>(options, toScore, crop);
        if (scr > topScore || topCrop.isEmpty()) {
            topCrop = crop;
            topScore = scr;
        }
    }
    if (s_benchmark) qDebug() << "crops scored in" << timer.elapsed() << "ms";

    return QRectF(topCrop.x() / prescale, topCrop.y() / prescale, topCrop.width() / prescale, topCrop.height() / prescale).toAlignedRect();
}

QImage crop(const QImage &input, CropOptions options)
{
    if (input.isNull()) {
        qWarning() << "Invalid image";
        return input;
    }
    return input.copy(smartCropRect(input, options));

}

} // namespace SmartCrop
