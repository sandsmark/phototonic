#include "Transform.h"

#include <QFile>
#include <QImageReader>
#include <QSaveFile>

extern "C" {
#include <turbojpeg.h>
}

bool losslessTransformJPEG(const QString &filename, const QImageIOHandler::Transformation transform, const QRect &crop, QString *error, bool perfect)
{
    {
        QImageReader testReader(filename);
        if (!testReader.canRead() || testReader.format() != "jpeg") {
            *error = QObject::tr("Not a jpeg file, only jpeg images are supported for now");
            return false;
        }
    }


    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        *error = "Failed to open " + filename;
        return false;
    }
    QByteArray fileContent = file.readAll();
    file.close();

    tjhandle tjInstance = tjInitTransform();
    if (!tjInstance) {
        *error = QObject::tr("Failed to allocate transform instance");
        return false;
    }

    tjtransform xform{};

    if (!crop.isEmpty()) {
        const QRect rect = crop.normalized();
        xform.r.x = rect.x();
        xform.r.y = rect.y();
        xform.r.h = rect.height();
        xform.r.w = rect.width();
        xform.options |= TJXOPT_CROP;
    }

    switch(transform) {
    case QImageIOHandler::TransformationRotate90:
        xform.op |= TJXOP_ROT90;
        break;
    case QImageIOHandler::TransformationRotate180:
        xform.op |= TJXOP_ROT180;
        break;
    case QImageIOHandler::TransformationRotate270:
        xform.op |= TJXOP_ROT270;
        break;
    case QImageIOHandler::TransformationFlip:
        xform.op |= TJXOP_VFLIP;
        break;
    case QImageIOHandler::TransformationMirror:
        xform.op |= TJXOP_HFLIP;
        break;
    case QImageIOHandler::TransformationMirrorAndRotate90:
        xform.op |= TJXOP_TRANSPOSE;
        break;
    case QImageIOHandler::TransformationFlipAndRotate90:
        xform.op |= TJXOP_TRANSVERSE;
        break;
    default:
        *error = QObject::tr("Transform not implemented");
        return false;
    }

    xform.options |= perfect ? TJXOPT_PERFECT : TJXOPT_TRIM;
    qDebug() << "transform:" << xform.op << xform.options << "buffer size:" << fileContent.size();

    unsigned char *dstBuf = nullptr;  /* Dynamically allocate the JPEG buffer */
    unsigned long dstSize = 0;

    int ret = tjTransform(tjInstance, (const uchar*)fileContent.data(), fileContent.size(), 1, &dstBuf, &dstSize,
                &xform, TJFLAG_ACCURATEDCT);
    if (ret < 0) {
        tjFree(dstBuf);
        *error = QString::fromLocal8Bit(tjGetErrorStr2(tjInstance));
        if (ret != -1) {
            *error += " (" + QString::number(ret) + ")";
        }
        tjDestroy(tjInstance);
        return false;
    }
    tjDestroy(tjInstance);

    // Will not overwrite until we call commit()
    QSaveFile tempFile(filename);
    if (!tempFile.open(QIODevice::WriteOnly)) {
        tjFree(dstBuf);
        *error = QObject::tr("Failed to open file for writing");
        return false;
    }
    int written = tempFile.write((const char*)dstBuf, dstSize);
    tjFree(dstBuf);
    if (written <= 0 || written != int(dstSize)) {
        *error = "Failed to write data, only wrote " + QString::number(written) + " but expected " + QString::number(dstSize);
        return false;
    }
    tempFile.commit();

    return true;
}

